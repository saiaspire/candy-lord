//Tey Jun Hong U095074X
//Arison Neo A0077784L
//Muhammad Azri A0073951B 

// ------------------------------------------------------------------
// Character Prototype
// ------------------------------------------------------------------
var PoliceObj = function(pos_x, pos_y, radius, id){
	// Set Parameters of characters
	this.pos_x = pos_x;
	this.pos_y = pos_y;
	this.radius = radius;
	this.grid = -1;	// ID of grid
	this.moveTimer = 0;
	this.townID = 0;	// Town which this character is in
	this.pubID = 0;		// Pub which this character is in
	this.state = 0;		// State of obj: 0: movable, 1: stay
	this.id = id;   //this is type
	this.keyID = 0;
	this.row = 0;
	this.col = 0;
	this.chase = false;
	this.leave = false;
	this.recognized = false;
	this.escapeDoor = 999;
	this.speed = 100;
	this.destroyTimer = Date.now()/1000;
	this.bribeAmount = 0;			// Bribe amount of police
	this.bribeDeviation1 = 0;		// Bribe amount deviation
	this.bribeDeviation2 = 0;		// Bribe amount deviation
	this.bribeDeviation3 = 0;		// Bribe amount deviation
	this.bribeDeviation = 0; 		// The chosen Bribe
	this.dialogueFlag = 0;			// Set stages of dialogue
	return this;
};
	
PoliceObj.prototype = {	
	// Draw circle for now
	draw : function(){
		if(gameLevel == 1||1){
			ctx.beginPath();
			ctx.arc(this.pos_x, this.pos_y, this.radius, 0, 2 * Math.PI, false);
			ctx.fillStyle = 'orange';
			ctx.fill();
			ctx.strokeStyle = '#003300';
			ctx.stroke();
			ctx.closePath();
		
			// The Face
			ctx.strokeStyle = '#0000FF';
			ctx.fillStyle = 'orange';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.pos_x,this.pos_y,this.radius,0*Math.PI,2*Math.PI,true);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
			
			ctx.beginPath();
			ctx.strokeStyle = "#000";
			ctx.fillStyle   = '#000'; 
			ctx.font = "normal 20px Verdana";	
			if(this.recognized === true)	
			{ctx.fillText("P", this.pos_x-8, this.pos_y+8);}	
			ctx.closePath();				
		}
	},
	
	// Move the player
	move : function(){	
		if(this.state == 0){
			if(gameLevel == 1||1){
				this.pushGridPos();
				this.moveTimer++;
				if(this.moveTimer % this.speed == 0){
					this.row = pathfinding.getObjectIndexRow(this);
					this.col = pathfinding.getObjectIndexCol(this);					
					var fate = Math.floor(Math.random()*4)+1;
					if(this.leave)	
						{fate = 6;}				
					if(this.chase)
						{fate = 5;}
					// endpoint = this.pos_y or this.pos_x +-= gridSize; depending on which fate				
					if(fate == 1){ 
						//goup
						if(this.pos_y > gridSize){ 
							pathfinding.objectgo(this,map[this.row][this.col-1].pos);
						}
					}
						//goleft
					if(fate == 2){ 
						if(this.pos_x > gridSize){ 
							pathfinding.objectgo(this,map[this.row-1][this.col].pos);
						}
					}
						//goright
					if(fate == 3){ 
						if(this.pos_x < canvas.width-gridSize){ 
							pathfinding.objectgo(this,map[this.row+1][this.col].pos);
						}
					}
						//godown
					if(fate == 4){ 
						if(this.pos_y < canvas.height-gridSize){ 
							pathfinding.objectgo(this,map[this.row][this.col+1].pos);
						}
					}		
					if(fate == 5){
						//alert("i am chasing"); 
						if(collision.distanceFrom(this,player)<=gridSize)
							{playerHealth-=1;}
						pathfinding.objectgo(this,player);	
					}
					if(fate == 6){
						//alert("i am leaving"); 
						if(this.escapeDoor===999)
						{this.escapeDoor = Math.floor(Math.random()*(doorOccupiedArray.length-0.5));}
						pathfinding.objectgo(this,map[doorOccupiedArray[this.escapeDoor].x][doorOccupiedArray[this.escapeDoor].y].pos);	
					}											
					//this.checkPos();
				}
			}
		}
	},
	
	// A check on the player's grid collision based on the position he is on
	// Diagonal vertices can actually be removed now as it is not needed
	checkPos : function(){
		// NSEW vertices for player
		var N = math.vector(this.pos_x, this.pos_y - this.radius);
		var S = math.vector(this.pos_x, this.pos_y + this.radius);
		var E = math.vector(this.pos_x + this.radius, this.pos_y);
		var W = math.vector(this.pos_x - this.radius, this.pos_y);
		var NE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));
		var NW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));		
		var SE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));
		var SW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));		
		var Center = math.vector(this.pos_x, this.pos_y);
		
		// Vertices collision flag
		var Npos = math.getGridPos(N.x, N.y);
		var Spos = math.getGridPos(S.x, S.y);
		var Epos = math.getGridPos(E.x, E.y);
		var Wpos = math.getGridPos(W.x, W.y);
		var NEpos = math.getGridPos(NE.x, NE.y);
		var NWpos = math.getGridPos(NW.x, NW.y);
		var SEpos = math.getGridPos(SE.x, SE.y);
		var SWpos = math.getGridPos(SW.x, SW.y);
		var Centerpos = math.getGridPos(Center.x, Center.y);
		
		// Check the grid it collide with and store in posArray
		var posArray = new Array();		
		posArray = collision.checkPlayerGridCollision(Npos,Spos,Epos,Wpos,NEpos,NWpos,SEpos,SWpos,Centerpos);	
		for(var i=0;i<posArray.length;i++){
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = '#FFFFFF'; 
			db.font = "normal 14px Verdana";		
			db.fillText(posArray[i], 12+i*debug.width/5, 5*debug.height/20);	
			db.closePath();					
		}	
	},
	
	// Push grid pos to a global array so that the controller has a bird eye view
	pushGridPos : function(){
		var tempgrid = math.getGridPos(this.pos_x, this.pos_y)
		// Splice the previous grid out from gridOccupiedArray
		if(this.grid > 0){
			for(var i=0;i<gridOccupiedArray.length;i++){
				if(gridOccupiedArray[i].grid == this.grid){
					gridOccupiedArray.splice(i, 1);						
				}else{
				
				}
			}
		}				
		this.grid = tempgrid;
		// Then push new grid into gridOccupiedArray
		gridOccupiedArray.push({id : this.id , grid : this.grid,keyID : this.keyID});
		//console.log(gridOccupiedArray);
		
		// Store new position inside map
  		var row = Math.floor(this.pos_x/gridSize);
  		var col = Math.floor(this.pos_y/gridSize);
		map[row][col].occupied = 1;
		map[row][col].id = this.id;
		map[row][col].keyID=this.keyID;
	},
	
	// Get Simulation Data
	getSimulationData : function(){
		var policeInfo = {
			bribeAmount : this.bribeAmount
		};	
		return policeInfo;
	},
	
	// Dialogue of the object
	dialogue : function(){

	//	console.log("State:"+this.state+"ID:"+this.keyID);
		if(this.state == 1){
			if(this.dialogueFlag == 1){
				this.recognized=true;
				ctx.beginPath();
				ctx.fillStyle="white";
				ctx.rect(5,canvas.height-100,canvas.width-10,90);
				ctx.fill();
				ctx.lineWidth = 4;
				ctx.strokeStyle = 'red';				
				ctx.stroke();
				ctx.closePath();
			
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("Team Rocket Police: Surrender Now or prepare to die", 20, canvas.height-60);	
				ctx.closePath();
				gameboard.whoseID = this.keyID;
			}
			if(this.dialogueFlag == 2){
				if(this.bribeDeviation1 == 0){
					this.bribeDeviation1 = Math.floor(Math.random()*this.bribeAmount*2) - this.bribeAmount;
					this.bribeDeviation1 = this.bribeDeviation1 + this.bribeAmount;
				}
				if(this.bribeDeviation2 == 0){
					this.bribeDeviation2 = Math.floor(Math.random()*this.bribeAmount*2) - this.bribeAmount;
					this.bribeDeviation2 = this.bribeDeviation2 + this.bribeAmount;
				}
				if(this.bribeDeviation3 == 0){
					this.bribeDeviation3 = Math.floor(Math.random()*this.bribeAmount*2) - this.bribeAmount;
					this.bribeDeviation3 = this.bribeDeviation3 + this.bribeAmount;
				}
				ctx.beginPath();
				ctx.fillStyle="white";
				ctx.rect(5,canvas.height-100,canvas.width-10,90);
				ctx.fill();
				ctx.lineWidth = 4;
				ctx.strokeStyle = 'red';				
				ctx.stroke();
				ctx.closePath();
			
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("Team Rocket Police: Bribe or Die", 20, canvas.height-60);	
				ctx.fillText("--> " + this.bribeDeviation1 + "                    " + this.bribeDeviation2 + "                    " + this.bribeDeviation3, 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 3){
				ctx.beginPath();
				ctx.fillStyle="white";
				ctx.rect(5,canvas.height-100,canvas.width-10,90);
				ctx.fill();
				ctx.lineWidth = 4;
				ctx.strokeStyle = 'red';				
				ctx.stroke();
				ctx.closePath();
			
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("Team Rocket Police: Bribe or Die", 20, canvas.height-60);	
				ctx.fillText(this.bribeDeviation1 + "                    " + "--> " +  this.bribeDeviation2 + "                    " + this.bribeDeviation3, 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 4){
				ctx.beginPath();
				ctx.fillStyle="white";
				ctx.rect(5,canvas.height-100,canvas.width-10,90);
				ctx.fill();
				ctx.lineWidth = 4;
				ctx.strokeStyle = 'red';				
				ctx.stroke();
				ctx.closePath();
			
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("Team Rocket Police: Bribe or Die", 20, canvas.height-60);	
				ctx.fillText(this.bribeDeviation1 + "                    " + this.bribeDeviation2 + "                    " + "--> " +  this.bribeDeviation3, 20, canvas.height-30);	
				ctx.closePath();
			}
			// Press ENTER go to Dialog 2
			if(13 in keysDown){
				delete keysDown[13];
				if(this.dialogueFlag == 0){
					this.dialogueFlag = 1;
				}else if(this.dialogueFlag == 1){
					this.dialogueFlag = 2;
				}else if(this.dialogueFlag > 1){
					if(this.dialogueFlag == 2){
						amount = amount - this.bribeDeviation1;
						this.bribeDeviation = this.bribeDeviation1;
					}else if(this.dialogueFlag == 3){
						amount = amount - this.bribeDeviation2;
						this.bribeDeviation = this.bribeDeviation2;
					}else if(this.dialogueFlag == 4){
						amount = amount - this.bribeDeviation3;		
						this.bribeDeviation = this.bribeDeviation3;			
					}	
					// Reset flag
					this.bribeDeviation1 = 0;		// Bribe amount deviation
					this.bribeDeviation2 = 0;		// Bribe amount deviation
					this.bribeDeviation3 = 0;		// Bribe amount deviation
					this.dialogueFlag = 0;			// Set stages of dialogue	
									
					// Check if the player is playing more than the bribe amount
					if(this.bribeDeviation >= this.bribeAmount){
						// Do nothing
						this.chase = false;
						this.speed = 100;
					}else{
						// Chase player
						//alert(" Row:"+this.row+"  Col:"+this.col);
						if(collision.distanceFrom(this,player)<gridSize*2){
							this.chase = true;
							this.speed = 30;
							//alert(this.chase);
						}	
					}
					player.state = 0;
					this.state = 0;									 
				}
			}
			
			// Press Left
			if(37 in keysDown && this.dialogueFlag > 1){
				delete keysDown[37];
				if(this.dialogueFlag == 2){
					
				}else if(this.dialogueFlag == 3){
					this.dialogueFlag = 2;
				}else if(this.dialogueFlag == 4){
					this.dialogueFlag = 3;					
				}
			}
			
			// Press Right 
			if(39 in keysDown && this.dialogueFlag > 1){
				delete keysDown[39];
				if(this.dialogueFlag == 2){
					this.dialogueFlag = 3;					
				}else if(this.dialogueFlag == 3){
					this.dialogueFlag = 4;
				}else if(this.dialogueFlag == 4){
								
				}
			}
		}
	}
};