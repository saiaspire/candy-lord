//Tey Jun Hong U095074X
//Arison Neo A0077784L
//Muhammad Azri A0073951B 


// ------------------------------------------------------------------
// Character Prototype
// ------------------------------------------------------------------
var DealerObj = function(pos_x, pos_y, radius, id){
	// Set Parameters of characters
	this.pos_x = pos_x;
	this.pos_y = pos_y;
	this.radius = radius;
	this.grid = -1;	// ID of grid
	this.moveTimer = 0;
	this.townID = 0;	// Town which this character is in
	this.pubID = 0;		// Pub which this character is in
	this.state = 0;		// State of obj: 0: movable, 1: stay
	this.id = id;   //this is type
	this.keyID = 0;
	this.row = 0;
	this.col = 0;
	this.speed = 80;
	this.leave = false;
	this.recognized = false;
	this.escapeDoor = 999;
	this.candy1 = 0;	// Quantity of candies
	this.candy2 = 0;
	this.candy3 = 0;
	this.candy4 = 0;
	this.candy1Price = 0;	// Quantity of candies
	this.candy2Price = 0;
	this.candy3Price = 0;
	this.candy4Price = 0;
	this.dialogueFlag = 0;	// Set stages of dialogue
	this.dialogueAction = 0;
	this.candyChosen = 0;
	this.quantityChosen = 0;
	this.rumourAcount = 0;	// Number of times the person is in contact with rumour A
	this.rumourBcount = 0;
	this.rumourCcount = 0;
	this.rumourDcount = 0;
	this.rumourHeld = "rumourAcount";
	this.rumourProb = 0;
	this.rumourThres = 0;
	return this;
};
	
DealerObj.prototype = {	
	// Get the max rumor
	whichRumor : function(){
		var maxRumor = Math.max(this.rumourAcount,this.rumourBcount,this.rumourCcount,this.rumourDcount);		
		if(maxRumor == this.rumourAcount)
			{this.rumourHeld="rumourAcount";}
		if(maxRumor == this.rumourBcount)
			{this.rumourHeld="rumourBcount";}
		if(maxRumor == this.rumourCcount)
			{this.rumourHeld="rumourCcount";}
		if(maxRumor == this.rumourDcount)
			{this.rumourHeld="rumourDcount";}	
			return this.rumourHeld;							
	},	
	// Draw circle for now
	draw : function(){
		if(gameLevel == 1||1){
			ctx.beginPath();
			ctx.arc(this.pos_x, this.pos_y, this.radius, 0, 2 * Math.PI, false);
			ctx.fillStyle = 'orange';
			ctx.fill();
			ctx.strokeStyle = '#003300';
			ctx.stroke();
			ctx.closePath();
		
			// The Face
			ctx.strokeStyle = '#0000FF';
			ctx.fillStyle = 'orange';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.pos_x,this.pos_y,this.radius,0*Math.PI,2*Math.PI,true);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
			
			ctx.beginPath();
			ctx.strokeStyle = "#000";
			ctx.fillStyle   = '#000'; 
			ctx.font = "normal 20px Verdana";		
			if(this.recognized === true)
			{ctx.fillText("D", this.pos_x-8, this.pos_y+8);}	
			ctx.closePath();				
		}
	},
	
	// Move the player
	move : function(){	
		if(this.state == 0){
			if(gameLevel == 1||1){
				this.pushGridPos();
				this.moveTimer++;
				if(this.moveTimer % this.speed == 0){
					this.row = pathfinding.getObjectIndexRow(this);
					this.col = pathfinding.getObjectIndexCol(this);		
					this.candy1 = Math.floor(this.candy1);	
					this.candy2 = Math.floor(this.candy2);
					this.candy3 = Math.floor(this.candy3);
					this.candy4 = Math.floor(this.candy4);
					this.candy1Price = Math.round(this.candy1Price);	
					this.candy2Price = Math.round(this.candy2Price);
					this.candy3Price = Math.round(this.candy3Price);
					this.candy4Price = Math.round(this.candy4Price);			
					var fate = Math.floor(Math.random()*4)+1;
					if(eval("this."+this.whichRumor())>this.rumourThres && player.state ===0)
					{
						console.log("keyId:"+this.keyID+"   "+eval("this."+this.whichRumor()));
						this.leave=true;
					}
					// endpoint = this.pos_y or this.pos_x +-= gridSize; depending on which fate	
					if(this.leave)
					{fate=6;}			
					if(fate == 1){ 
						//goup
						if(this.pos_y > gridSize){ 
							pathfinding.objectgo(this,map[this.row][this.col-1].pos);
						}
					}
						//goleft
					if(fate == 2){ 
						if(this.pos_x > gridSize){ 
							pathfinding.objectgo(this,map[this.row-1][this.col].pos);
						}
					}
						//goright
					if(fate == 3){ 
						if(this.pos_x < canvas.width-gridSize){ 
							pathfinding.objectgo(this,map[this.row+1][this.col].pos);
						}
					}
						//godown
					if(fate == 4){ 
						if(this.pos_y < canvas.height-gridSize){ 
							pathfinding.objectgo(this,map[this.row][this.col+1].pos);
						}
					}
					if(fate == 6){
						//alert("i am leaving"); 
						if(this.escapeDoor===999)
						{this.escapeDoor = Math.floor(Math.random()*(doorOccupiedArray.length-0.5));}
						console.log("Escape door"+this.escapeDoor);
						pathfinding.objectgo(this,map[doorOccupiedArray[this.escapeDoor].x][doorOccupiedArray[this.escapeDoor].y].pos);	
					}								
					//this.checkPos();
				}
			}
		}
	},
	
	// A check on the player's grid collision based on the position he is on
	// Diagonal vertices can actually be removed now as it is not needed
	checkPos : function(){
		// NSEW vertices for player
		var N = math.vector(this.pos_x, this.pos_y - this.radius);
		var S = math.vector(this.pos_x, this.pos_y + this.radius);
		var E = math.vector(this.pos_x + this.radius, this.pos_y);
		var W = math.vector(this.pos_x - this.radius, this.pos_y);
		var NE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));
		var NW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));		
		var SE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));
		var SW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));		
		var Center = math.vector(this.pos_x, this.pos_y);
		
		// Vertices collision flag
		var Npos = math.getGridPos(N.x, N.y);
		var Spos = math.getGridPos(S.x, S.y);
		var Epos = math.getGridPos(E.x, E.y);
		var Wpos = math.getGridPos(W.x, W.y);
		var NEpos = math.getGridPos(NE.x, NE.y);
		var NWpos = math.getGridPos(NW.x, NW.y);
		var SEpos = math.getGridPos(SE.x, SE.y);
		var SWpos = math.getGridPos(SW.x, SW.y);
		var Centerpos = math.getGridPos(Center.x, Center.y);
		
		// Check the grid it collide with and store in posArray
		var posArray = new Array();		
		posArray = collision.checkPlayerGridCollision(Npos,Spos,Epos,Wpos,NEpos,NWpos,SEpos,SWpos,Centerpos);	
		for(var i=0;i<posArray.length;i++){
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = '#FFFFFF'; 
			db.font = "normal 14px Verdana";		
			db.fillText(posArray[i], 12+i*debug.width/5, 5*debug.height/20);	
			db.closePath();					
		}	
	},
	
	// Push grid pos to a global array so that the controller has a bird eye view
	pushGridPos : function(){
		var tempgrid = math.getGridPos(this.pos_x, this.pos_y)
		// Splice the previous grid out from gridOccupiedArray
		if(this.grid > 0){
			for(var i=0; i<gridOccupiedArray.length;i++){
				if(gridOccupiedArray[i].grid == this.grid){
					gridOccupiedArray.splice(i, 1);	
					break;					
				}else{
				
				}
			}
		}				
		this.grid = tempgrid;
		// Then push new grid into gridOccupiedArray
		gridOccupiedArray.push({id : this.id , grid : this.grid,keyID : this.keyID});
		//console.log(gridOccupiedArray);
		
		// Store new position inside map
  		var row = Math.floor(this.pos_x/gridSize);
  		var col = Math.floor(this.pos_y/gridSize);
		map[row][col].occupied = 1;
		map[row][col].id = this.id;
		map[row][col].keyID=this.keyID;
	},
	
	
	// Get Simulation Data
	getSimulationData : function(){
		var dealerInfo = {
			rumourAcount : this.rumourAcount,
			rumourBcount : this.rumourBcount,
			rumourCcount : this.rumourCcount,
			rumourDcount : this.rumourDcount,
			rumourThres : this.rumourThres ,
			rumourProb : this.rumourProb,
			candy1	: this.candy1,
			candy2	: this.candy2,
			candy3	: this.candy3,
			candy4	: this.candy4,
			candy1Price : this.candy1Price,	// Quantity of candies
			candy2Price : this.candy2Price,	// Quantity of candies
			candy3Price : this.candy3Price,	// Quantity of candies
			candy4Price : this.candy4Price,	// Quantity of candies
		};	
		return dealerInfo;
	},
	
	// Dialogue of the object
	dialogue : function(){
		if(this.state == 1){
			// Initialise dialogue box
			ctx.beginPath();
			ctx.fillStyle="white";
			ctx.rect(5,canvas.height-100,canvas.width-10,90);
			ctx.fill();
			ctx.lineWidth = 4;
			ctx.strokeStyle = 'red';				
			ctx.stroke();
			ctx.closePath();
		
			ctx.beginPath();
			ctx.strokeStyle = "#000";
			ctx.fillStyle   = 'black'; 
			ctx.font = "normal 25px Verdana";
			if(this.dialogueFlag == 1){
				this.recognized=true;
				var rumourText = 0;
				if(this.rumourAcount > this.rumourBcount && this.rumourAcount > this.rumourCcount && this.rumourAcount > this.rumourDcount){
					rumourText = "Rumour A";
				}else if(this.rumourBcount > this.rumourAcount && this.rumourBcount > this.rumourCcount && this.rumourBcount > this.rumourDcount){
					rumourText = "Rumour B";				
				}else if(this.rumourCcount > this.rumourAcount && this.rumourCcount > this.rumourBcount && this.rumourCcount > this.rumourDcount){
					rumourText = "Rumour C";				
				}else if(this.rumourDcount > this.rumourAcount && this.rumourDcount > this.rumourBcount && this.rumourDcount > this.rumourCcount){
					rumourText = "Rumour D";				
				}else{
					rumourText = "No Rumor";					
				}
		
				ctx.fillText("Dealer: " + rumourText, 20, canvas.height-60);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 2){	
				ctx.fillText("Dealer: Buy or Sell?", 20, canvas.height-60);	
				ctx.fillText("--> " + "Buy" + "                    " + "Sell", 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 3){	
				ctx.fillText("Dealer: Buy or Sell?", 20, canvas.height-60);	
				ctx.fillText("Buy" + "                    " + "--> " + "Sell", 20, canvas.height-30);	
				ctx.closePath();
			}
			// Dealer's candy worth
			if(this.dialogueFlag == 4){	
				ctx.fillText("Price", 20, canvas.height-60);	
				ctx.font = "normal 15px Verdana";
				ctx.fillText("--> " + "CandyA--$" + this.candy1Price + "     " + "CandyB--$" + this.candy2Price + "     " + "CandyC--$" + this.candy3Price + "     " + "CandyD--$" + this.candy4Price, 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 5){	
				ctx.fillText("Price", 20, canvas.height-60);	
				ctx.font = "normal 15px Verdana";
				ctx.fillText("CandyA--$" + this.candy1Price + "     " + "--> " + "CandyB--$" + this.candy2Price + "     " + "CandyC--$" + this.candy3Price + "     " + "CandyD--$" + this.candy4Price, 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 6){	
				ctx.fillText("Price", 20, canvas.height-60);	
				ctx.font = "normal 15px Verdana";
				ctx.fillText("CandyA--$" + this.candy1Price + "     " + "CandyB--$" + this.candy2Price + "     " + "--> " + "CandyC--$" + this.candy3Price + "     " + "CandyD--$" + this.candy4Price, 20, canvas.height-30);	
				ctx.closePath();
			}
			if(this.dialogueFlag == 7){	
				ctx.fillText("Price", 20, canvas.height-60);	
				ctx.font = "normal 15px Verdana";
				ctx.fillText("CandyA--$" + this.candy1Price + "     " + "CandyB--$" + this.candy2Price + "     " + "CandyC--$" + this.candy3Price + "     " + "--> " +  "CandyD--$" + this.candy4Price, 20, canvas.height-30);	
				ctx.closePath();
			}
			
			// Quantity Choosing
			if(this.dialogueFlag == 8){
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("Choose a Quantity", 20, canvas.height-60);	
				ctx.fillText(this.quantityChosen, 20, canvas.height-30);	
				ctx.closePath();
			}
			
			// Not enough Money
			if(this.dialogueFlag == 9){		
				ctx.fillText("Not Enough Cash", 20, canvas.height-60);	
				ctx.fillText("Dealer: Go sell some candies", 20, canvas.height-30);	
				ctx.closePath();
			}
			
			// Transaction Successful
			if(this.dialogueFlag == 10){		
				ctx.fillText("Transaction Successful", 20, canvas.height-60);	
				ctx.fillText("Dealer: Have a Nice Trade Day Ahead", 20, canvas.height-30);	
				ctx.closePath();
			}
			
			// Out of Stock
			if(this.dialogueFlag == 11){
				ctx.fillText("Out of Stock", 20, canvas.height-60);	
				ctx.fillText("Dealer: Sorry Bro, I do not have what you need", 20, canvas.height-30);	
				ctx.closePath();			
			}
			
			// Press ENTER go to Dialog 2
			if(13 in keysDown){
				delete keysDown[13];
				if(this.dialogueFlag == 0){
					this.dialogueFlag = 1;
				}else if(this.dialogueFlag == 1){
					this.dialogueFlag = 2;
				}else if(this.dialogueFlag > 1 && this.dialogueFlag < 4){
					if(this.dialogueFlag == 2){
						this.dialogueAction = 1;	// Buy
						this.dialogueFlag = 4;
					}else if(this.dialogueFlag == 3){
						this.dialogueAction = 2;	// Sell
						this.dialogueFlag = 4;
					}								 
				}else if(this.dialogueFlag > 3 && this.dialogueFlag < 8){
					// Candy price
					if(this.dialogueFlag == 4){
						this.candyChosen = 1;
						this.dialogueFlag = 8;
						if(this.dialogueAction == 1){
							if(this.candy1 == 0){
								this.dialogueFlag = 11;	// Out of stock
							}
						}
					}else if(this.dialogueFlag == 5){
						this.candyChosen = 2;
						this.dialogueFlag = 8;
						if(this.dialogueAction == 1){
							if(this.candy2 == 0){
								this.dialogueFlag = 11;	// Out of stock
							}
						}
					}else if(this.dialogueFlag == 6){
						this.candyChosen = 3;
						this.dialogueFlag = 8;
						if(this.dialogueAction == 1){
							if(this.candy3 == 0){
								this.dialogueFlag = 11;	// Out of stock
							}
						}
					}else if(this.dialogueFlag == 7){
						this.candyChosen = 4;
						this.dialogueFlag = 8;
						if(this.dialogueAction == 1){
							if(this.candy4 == 0){
								this.dialogueFlag = 11;	// Out of stock
							}
						}
					}	
				}else if(this.dialogueFlag == 8){	
					// Buy / Sell result
					if(this.dialogueAction == 1){	// Buy
						var price = 0;
						if(this.candyChosen == 1){
							price = this.candy1Price;		
						}else if(this.candyChosen == 2){
							price = this.candy2Price;
						}else if(this.candyChosen == 3){
							price = this.candy3Price;
						}else if(this.candyChosen == 4){
							price = this.candy4Price;
						}
						var totalPrice = this.quantityChosen * price;
						if(totalPrice > amount){
							// Not enough money
							this.dialogueFlag = 9;
						}else{
							// Success
							this.dialogueFlag = 10;	
							// Change the candy amount for player
							if(this.candyChosen == 1){
								player.candy1 = player.candy1 + this.quantityChosen;		
							}else if(this.candyChosen == 2){
								player.candy2 = player.candy2 + this.quantityChosen;
							}else if(this.candyChosen == 3){
								player.candy3 = player.candy3 + this.quantityChosen;
							}else if(this.candyChosen == 4){
								player.candy4 = player.candy4 + this.quantityChosen;
							}		
							// Change the candy amount for dealer
							if(this.candyChosen == 1){
								this.candy1 = this.candy1 - this.quantityChosen;		
							}else if(this.candyChosen == 2){
								this.candy2 = this.candy2 - this.quantityChosen;
							}else if(this.candyChosen == 3){
								this.candy3 = this.candy3 - this.quantityChosen;
							}else if(this.candyChosen == 4){
								this.candy4 = this.candy4 - this.quantityChosen;
							}
							// Change the cash amount					
							amount = amount - totalPrice;					
						}
					}else if(this.dialogueAction == 2){	//Sell
						// Change the cash amount	
						var price = 0;
						if(this.candyChosen == 1){
							price = this.candy1Price;
						}else if(this.candyChosen == 2){
							price = this.candy2Price;
						}else if(this.candyChosen == 3){
							price = this.candy3Price;
						}else if(this.candyChosen == 4){
							price = this.candy4Price;
						}
						var totalPrice = this.quantityChosen * price;
						amount = amount + totalPrice;
						// Change the candy amount
						if(this.candyChosen == 1){
							player.candy1 = player.candy1 - this.quantityChosen;		
						}else if(this.candyChosen == 2){
							player.candy2 = player.candy2 - this.quantityChosen;
						}else if(this.candyChosen == 3){
							player.candy3 = player.candy3 - this.quantityChosen;
						}else if(this.candyChosen == 4){
							player.candy4 = player.candy4 - this.quantityChosen;
						}
						// Change the candy amount for dealer
						if(this.candyChosen == 1){
							this.candy1 = this.candy1 + this.quantityChosen;		
						}else if(this.candyChosen == 2){
							this.candy2 = this.candy2 + this.quantityChosen;
						}else if(this.candyChosen == 3){
							this.candy3 = this.candy3 + this.quantityChosen;
						}else if(this.candyChosen == 4){
							this.candy4 = this.candy4 + this.quantityChosen;
						}	
						this.dialogueFlag = 10;
					}
				
				}else if(this.dialogueFlag > 8){								
					// Reset flag
					this.dialogueFlag = 0;			// Set stages of dialogue	
					this.dialogueAction = 0;
					this.candyChosen = 0;
					this.quantityChosen = 0;
					
					eval("this."+player.whichRumor()+"+=1");
					//alert("influence");
					player.state = 0;
					this.state = 0;					
				}
			}
			
			// Press Left
			if(37 in keysDown && this.dialogueFlag > 1){
				delete keysDown[37];
				// Buy / Sell
				if(this.dialogueFlag == 2){
					
				}else if(this.dialogueFlag == 3){
					this.dialogueFlag = 2;
				}
				
				// Candy Price
				if(this.dialogueFlag == 4){
					
				}else if(this.dialogueFlag == 5){
					this.dialogueFlag = 4;
				}else if(this.dialogueFlag == 6){
					this.dialogueFlag = 5;
				}else if(this.dialogueFlag == 7){
					this.dialogueFlag = 6;
				}
				
				// Quantity 
				if(this.dialogueFlag == 8){
					if(this.quantityChosen > 0){
						this.quantityChosen--;
					}
				}
			}
			
			// Press Right 
			if(39 in keysDown && this.dialogueFlag > 1){
				delete keysDown[39];
				// Buy / Sell
				if(this.dialogueFlag == 2){
					this.dialogueFlag = 3;					
				}else if(this.dialogueFlag == 3){
					
				}
				
				// Candy Price
				if(this.dialogueFlag == 4){
					this.dialogueFlag = 5;
				}else if(this.dialogueFlag == 5){
					this.dialogueFlag = 6;
				}else if(this.dialogueFlag == 6){
					this.dialogueFlag = 7;
				}else if(this.dialogueFlag == 7){
				
				}
				
				// Quantity 
				if(this.dialogueFlag == 8){
					// Restrict the quantity the player can sell
					var maxQuantity = 99;					
					if(this.dialogueAction == 2){	//Sell
						if(this.candyChosen == 1){
							maxQuantity = player.candy1;
						}else if(this.candyChosen == 2){
							maxQuantity = player.candy2;							
						}else if(this.candyChosen == 3){
							maxQuantity = player.candy3;							
						}else if(this.candyChosen == 4){
							maxQuantity = player.candy4;							
						}
					}
					if(this.dialogueAction == 1){	//Buy
						if(this.candyChosen == 1){
							maxQuantity = this.candy1;
						}else if(this.candyChosen == 2){
							maxQuantity = this.candy2;							
						}else if(this.candyChosen == 3){
							maxQuantity = this.candy3;							
						}else if(this.candyChosen == 4){
							maxQuantity = this.candy4;							
						}
					}
					if(this.quantityChosen < maxQuantity){
						this.quantityChosen++;
					}
				}
			}
		}	
	}
};