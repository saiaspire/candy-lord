//Tey Jun Hong U095074X
//Arison Neo A0077784L
//Muhammad Azri A0073951B 

// ------------------------------------------------------------------
// Character Prototype
// ------------------------------------------------------------------
var RobberObj = function(pos_x, pos_y, radius, id){
	// Set Parameters of characters
	this.pos_x = pos_x;
	this.pos_y = pos_y;
	this.radius = radius;
	this.grid = -1;	// ID of grid
	this.moveTimer = 0;
	this.state = 0;		// State of obj: 0: movable, 1: stay
	this.id = id;   //this is type
	this.keyID = 0;
	this.row = 0;
	this.col = 0;
	this.recognized = false;
	this.speed = 70;
	this.destroyTimer = Date.now()/1000;
	this.leave = false;
	this.escapeDoor = 999;
	this.steal = false;
	this.stealdealer=false;
	this.stealAmount = 0;	// The amount to steal
	this.stealType = 0;		// The candy type to steal, depends on probabilistic model
	return this;
};
	
RobberObj.prototype = {	
	// Draw circle for now
	draw : function(){
		if(gameLevel == 1||1){
			ctx.beginPath();
			ctx.arc(this.pos_x, this.pos_y, this.radius, 0, 2 * Math.PI, false);
			ctx.fillStyle = 'orange';
			ctx.fill();
			ctx.strokeStyle = '#003300';
			ctx.stroke();
			ctx.closePath();
		
			// The Face
			ctx.strokeStyle = '#0000FF';
			ctx.fillStyle = 'orange';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.pos_x,this.pos_y,this.radius,0*Math.PI,2*Math.PI,true);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
			
			ctx.beginPath();
			ctx.strokeStyle = "#000";
			ctx.fillStyle   = '#000'; 
			ctx.font = "normal 20px Verdana";	
			if(this.recognized === true)	
			{ctx.fillText("R", this.pos_x-8, this.pos_y+8);}	
			ctx.closePath();				
		}
	},
	
	// Move the player
	move : function(){	

		if(this.state == 0){
			if(gameLevel == 1 ||1){
				this.pushGridPos();							
				this.moveTimer++;
				if(this.moveTimer % this.speed == 0){
					this.row = pathfinding.getObjectIndexRow(this);
					this.col = pathfinding.getObjectIndexCol(this);					
					var fate = Math.floor(Math.random()*4)+1;
					// endpoint = this.pos_y or this.pos_x +-= gridSize; depending on which fate	
					if(Math.random()>0.98 && collision.distanceFrom(this,player)< gridSize*3 && this.steal!==true && this.stealdealer ===false)
					{
						//alert("stealing player!!");
						this.steal=true;
					}			
					if(Math.random()>0.98 && collision.distanceFrom(this,dealer[this.nearestDealerIndex()])< gridSize*3 && this.stealdealer!==true && this.steal===false)
					{
						//alert("stealing dealer!!"+this.nearestDealerIndex());
						this.stealdealer=true;
					}							
					if(this.steal)
					{fate = 5;}
					if(this.leave)
					{fate=6;}		
					if(this.stealdealer)
					{fate=7;}

					if(fate == 1){ 
						//goup
						if(this.pos_y > gridSize*2){ 
							pathfinding.objectgo(this,map[this.row][this.col-2].pos);
						}
					}
						//goleft
					if(fate == 2){ 
						if(this.pos_x > gridSize*2){ 
							pathfinding.objectgo(this,map[this.row-2][this.col].pos);
						}
					}
						//goright
					if(fate == 3){ 
						if(this.pos_x < canvas.width-gridSize*2){ 
							pathfinding.objectgo(this,map[this.row+2][this.col].pos);
						}
					}
						//godown
					if(fate == 4){ 
						if(this.pos_y < canvas.height-gridSize*2){ 
							pathfinding.objectgo(this,map[this.row][this.col+2].pos);
						}
					}
					if(fate == 5){

						pathfinding.objectgo(this,player);
						if(collision.distanceFrom(this,player) <= gridSize)
						{
							if(eval("player.candy"+this.stealType)>this.stealAmount)
							eval("player.candy"+this.stealType+"-=this.stealAmount");
							else
							{eval("player.candy"+this.stealType+"-=player.candy"+this.stealType);}
							this.leave=true;
						}	
					}						
					if(fate == 6){
						//alert("i am leaving"); 
						if(this.escapeDoor===999)
						{this.escapeDoor = Math.floor(Math.random()*(doorOccupiedArray.length-0.5));}
						this.steal=false;
						this.stealdealer=false;
						pathfinding.objectgo(this,map[doorOccupiedArray[this.escapeDoor].x][doorOccupiedArray[this.escapeDoor].y].pos);	
					}						
					if(fate == 7){
						var dIndex = this.nearestDealerIndex();
						pathfinding.objectgo(this,dealer[dIndex]);
						if(collision.distanceFrom(this,dealer[dIndex]) <= gridSize)
						{
							if(eval("dealer["+dIndex+"].candy"+this.stealType)>this.stealAmount)
							eval("dealer["+dIndex+"].candy"+this.stealType+"-=this.stealAmount");
							else
							{eval("dealer["+dIndex+"].candy"+this.stealType+"-=dealer["+dIndex+"].candy"+this.stealType);}
							this.leave=true;
						}	
					}				
				}
			}
		}
	},
	
	// A check on the player's grid collision based on the position he is on
	// Diagonal vertices can actually be removed now as it is not needed
	checkPos : function(){
		// NSEW vertices for player
		var N = math.vector(this.pos_x, this.pos_y - this.radius);
		var S = math.vector(this.pos_x, this.pos_y + this.radius);
		var E = math.vector(this.pos_x + this.radius, this.pos_y);
		var W = math.vector(this.pos_x - this.radius, this.pos_y);
		var NE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));
		var NW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));		
		var SE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));
		var SW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));		
		var Center = math.vector(this.pos_x, this.pos_y);
		
		// Vertices collision flag
		var Npos = math.getGridPos(N.x, N.y);
		var Spos = math.getGridPos(S.x, S.y);
		var Epos = math.getGridPos(E.x, E.y);
		var Wpos = math.getGridPos(W.x, W.y);
		var NEpos = math.getGridPos(NE.x, NE.y);
		var NWpos = math.getGridPos(NW.x, NW.y);
		var SEpos = math.getGridPos(SE.x, SE.y);
		var SWpos = math.getGridPos(SW.x, SW.y);
		var Centerpos = math.getGridPos(Center.x, Center.y);
		
		// Check the grid it collide with and store in posArray
		var posArray = new Array();		
		posArray = collision.checkPlayerGridCollision(Npos,Spos,Epos,Wpos,NEpos,NWpos,SEpos,SWpos,Centerpos);	
		for(var i=0;i<posArray.length;i++){
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = '#FFFFFF'; 
			db.font = "normal 14px Verdana";		
			db.fillText(posArray[i], 12+i*debug.width/5, 5*debug.height/20);	
			db.closePath();					
		}	
	},

	nearestDealerIndex : function(){
		var index=0;
		for(var i=0;i<dealer.length;i++)
		{
			if(collision.distanceFrom(dealer[index],this)>collision.distanceFrom(dealer[i],this) )
				{index=i;}
		}
		return index;
	},
	
	// Push grid pos to a global array so that the controller has a bird eye view
	pushGridPos : function(){
		var tempgrid = math.getGridPos(this.pos_x, this.pos_y)
		// Splice the previous grid out from gridOccupiedArray
		if(this.grid > 0){
			for(var i=0;i<gridOccupiedArray.length;i++){
				if(gridOccupiedArray[i].grid == this.grid){
					gridOccupiedArray.splice(i, 1);						
				}else{
				
				}
			}
		}				
		this.grid = tempgrid;
		// Then push new grid into gridOccupiedArray
		gridOccupiedArray.push({id : this.id , grid : this.grid,keyID : this.keyID});
		//console.log(gridOccupiedArray);
		
		// Store new position inside map
  		var row = Math.floor(this.pos_x/gridSize);
  		var col = Math.floor(this.pos_y/gridSize);
  		//console.log(row+":"+col);
		map[row][col].occupied = 1;
		map[row][col].id = this.id;
		map[row][col].keyID=this.keyID;
	},
	
	// Get Simulation Data
	getSimulationData : function(){
		var robberInfo = {
			stealAmount : this.stealAmount,
			stealType: this.stealType
		};
		return robberInfo;
	},
	
	// Dialogue of the object
	dialogue : function(){		
		if(this.state == 1){
			this.recognized=true;
			ctx.beginPath();
			ctx.fillStyle="white";
			ctx.rect(5,canvas.height-100,canvas.width-10,90);
			ctx.fill();
			ctx.lineWidth = 4;
			ctx.strokeStyle = 'red';				
			ctx.stroke();
			ctx.closePath();
			
			ctx.beginPath();
			ctx.strokeStyle = "#000";
			ctx.fillStyle   = 'black'; 
			ctx.font = "normal 25px Verdana";		
			ctx.fillText("Robber: What is that Pokemon?", 20, canvas.height-60);	
			ctx.closePath();	
			if(Math.random()>0.97)
				{
					this.steal=true;
				}

			if(13 in keysDown){
				player.state = 0;
				this.state = 0;				
			}
		}
	}
};