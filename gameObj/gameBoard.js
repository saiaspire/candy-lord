//Muhammad Azri A0073951B 
// ------------------------------------------------------------------
// Game Board
// ------------------------------------------------------------------
function gameBoard(){
	// Constructors
	this.initGrid = 0;
	this.toggleGrid = 0;
	this.initMapFlag = 0;
	this.softreset = 0;
	this.prevtownID = 0;
	this.prevpubID = 0;
	this.prevwhereAmI = "city";		
	this.mapSpawnFinish = false;
	this.tempPeople = new Array();
	this.cityspawnarea = new Array();
	this.townspawnarea = new Array();
	this.townspawnarea.push({row : 1,col : 4});
	this.townspawnarea.push({row : 5,col : 3});
	this.townspawnarea.push({row : 12,col : 3});
	this.townspawnarea.push({row : 12,col : 8});
	this.townspawnarea.push({row : 5,col : 8});

	this.cityspawnarea.push({row : 4,col : 6});
	this.cityspawnarea.push({row : 8,col : 2});
	this.cityspawnarea.push({row : 12,col : 5});
	this.cityspawnarea.push({row : 10,col : 9});
    this.columns=canvas.height/gridSize;
    this.rows=canvas.width/gridSize;
	//Particles
	this.mp = 100; //max particles
	this.angle = 0;
	this.spawnrate = 0;
	this.whoseID=0;
	this.prevSpawnTime = Date.now()/1000;
	this.particles = [];
	for(var i = 0; i < this.mp; i++){
		this.particles.push({
			x: Math.random()*canvas.width, //x-coordinate
			y: Math.random()*canvas.height, //y-coordinate
			r: Math.random()*2+1, //radius
			d: Math.random()*this.mp //density
		})
	}    
    
	// Draw gameBoard
	this.draw = function(){ 
		ctx.beginPath();
		ctx.rect(0,0,canvas.width,canvas.height);
		//ctx.fillStyle   = '#fbd685'; 
		//ctx.fill();
		ctx.closePath();
		if(73 in keysDown){ // Press I for debug mode
			this.toggleGrid = 1;
		}else{
			this.toggleGrid = 0;
		}	
		
		this.drawGrid();
		this.drawStats();
	}
	
	this.updateWeather = function(){
		if(gameLevel != 1){
			if(gameLevel == 2){
				this.mp = 50;
			}else if(gameLevel == 3){
				this.mp = 100;			
			}
			// Draw Particle
			ctx.beginPath();		
			ctx.fillStyle = "white";//"rgba(255, 255, 255, 0.8)";
			// Particle
			for(var i = 0; i < this.mp; i++){
				var p = this.particles[i];
				ctx.moveTo(p.x, p.y);
				ctx.arc(p.x, p.y, p.r, 0, Math.PI*2, true);
				//ctx.rect(p.x, p.y, 2,2);
				//ctx.lineTo(p.x,p.y+i*0.5);
			}
			//ctx.strokeStyle = "#0066CC";//"rgba(0,0,0, 0.8)";
			//ctx.stroke();
			ctx.fill();	
			ctx.closePath();		
	
			this.angle += 0.01;
			for(var i = 0; i < this.mp; i++){
				var p = this.particles[i];
				p.y += Math.cos(this.angle+p.d) + 1 + 0.1*p.r/2;
				p.x += Math.sin(this.angle);
			
				if(p.x > canvas.width+5 || p.x < -5 || p.y > canvas.height){
					if(i%3 > 0){ //66.67% 
						this.particles[i] = {x: Math.random()*canvas.width, y: -10, r: p.r, d: p.d};
					}else{
					//If the flake is exitting from the right
						if(Math.sin(this.angle) > 0){
							//Enter from the left
							this.particles[i] = {x: -5, y: Math.random()*canvas.height, r: p.r, d: p.d};
						}else{
							//Enter from the right
							this.particles[i] = {x: canvas.width+5, y: Math.random()*canvas.height, r: p.r, d: p.d};
						}
					}
				}
			}
		}
	}
	
	this.drawStats = function(){
		ctx.beginPath();
		ctx.fillStyle   = 'red'; 
		ctx.font = "normal 30px Verdana";		
		ctx.fillText("HP ", 1*canvas.width/55, 2*canvas.height/30);
		ctx.closePath();	
		ctx.beginPath();	
		for(var i=1; i<playerHealth+1; i++){
			ctx.fillRect(canvas.width/55*i+100, 1*canvas.height/20, 15, 5);
		}
		ctx.closePath();
		ctx.beginPath();	
		ctx.fillStyle   = 'black'; 
		ctx.font = "normal 30px Verdana";		
		ctx.fillText("$ "+ amount, 45*canvas.width/55, 2*canvas.height/30);
		ctx.closePath();	
	}

	// Draw Debug
	this.debugDraw = function(){
		db.beginPath();
		db.rect(0,0,debug.width,debug.height);
		db.fillStyle = "black";
		db.fill();
		db.closePath();
		this.debugdrawFPS();
		//this.debugdrawGrid();	// Comment this to disable grid
		this.debugdrawParameters();
	}
	
	// Draw Grid and push grid to global array
	this.showGrid = function(){
		// Number boxes
		var gridCount = 1;
		var gridInfo = new Array();
		for(var i=0; i<18; i++){
			for(var j=0; j<10; j++){
				if(this.toggleGrid == 1){
					ctx.beginPath();	
					ctx.strokeStyle = "#000000";
					ctx.fillStyle   = '#000000'; 
					ctx.font = "normal 14px Verdana";		
					ctx.fillText(gridCount, i*gridSize+gridSize/4, j*gridSize+gridSize/2);	
					ctx.closePath();
				}	
				gridCount++;
				if(this.initGrid == 0){
					gridInfo = {
						id : gridCount-1,
						upperLeftX : i*gridSize,	
						upperLeftY : j*gridSize,
						lowerRightX : i*gridSize+gridSize,
						lowerRightY : j*gridSize+gridSize,	
						centerX		: i*gridSize+gridSize/2,	
						centerY		: j*gridSize+gridSize/2,				
					}
					gridArray.push(gridInfo);
					map[i][j].pos.pos_x = i*gridSize+gridSize/2;
					map[i][j].pos.pos_y = j*gridSize+gridSize/2;
				}
			}
		}	
		this.initGrid = 1;	
	}
	
	this.drawGrid = function(){
		var gridCount = 1;
		if(this.toggleGrid == 1){
			ctx.strokeStyle = "black";
			ctx.lineWidth = 1;
			// Comment below for framerate optimisation
			// Draw vertical line
			for(var i=0; i<canvas.height; i+=gridSize){
				ctx.beginPath();
				ctx.moveTo(0,i);
				ctx.lineTo(canvas.width,i);
				ctx.stroke();
				ctx.closePath();
			}			
			// Draw horizontal line
			for(var i=0; i<canvas.width; i+=gridSize){
				ctx.beginPath();
				ctx.moveTo(i,0);
				ctx.lineTo(i,canvas.height);
				ctx.stroke();
				ctx.closePath();
			}
		}	
		for(var i=0; i<18; i++){
			for(var j=0; j<10; j++){
				if(this.toggleGrid == 1){
					ctx.beginPath();	
					ctx.strokeStyle = "#000000";
					ctx.fillStyle   = '#000000'; 
					ctx.font = "normal 14px Verdana";		
					ctx.fillText(gridCount, i*gridSize+gridSize/4, j*gridSize+gridSize/2);	
					ctx.closePath();
				}	
				gridCount ++;
			}
		}	
	}
	
	this.initMap = function(){
		var randomrow=0,randomcol=0;
		if(this.initMapFlag == 0){
			for(var x = 0; x < 18; x++){
				map[x] = [];
				for(var y = 0; y < 10; y++){
					map[x][y] = {
						id : 0,
						keyID : 0,						
						occupied : 0,
						pos : {
							pos_x : 0,
							pos_y : 0
						}
					}			
				}
			}	
		}else{
			for(var x = 0; x < 18; x++){
				for(var y = 0; y < 10; y++){
					if(this.softreset == 0)
					{
					this.mapSpawnFinish=false;


					if(x==0&&y==0)	
					{	

					if(this.prevwhereAmI === "pub")	
					{
						simulationArray.level1.id=this.prevpubID;
						simulationArray.level1.parentTownId = townID;
					}
					else
					{
						simulationArray.level1.id=this.prevtownID;	
					}
					simulationArray.level1.type = this.prevwhereAmI;
					//alert(this.prevwhereAmI);
					simulationArray.level1.robber=robber;
					simulationArray.level1.police=police;
					simulationArray.level1.dealer=dealer;

						var whereAmI = "nowhere";
						if(townID === 0 && pubID === 0)
							{whereAmI = "city";}
						else if(pubID === 0)
							{whereAmI = "town";}
						else
						{whereAmI="pub";}		

						if(this.prevwhereAmI == "city") // i am at town
						{
							//alert(townID);
							for(var o =0; o<simulationArray.level2.length;o++)
							{
								//alert(simulationArray.level2[o].id+":"+simulationArray.level2.length);
								if(townID == simulationArray.level2[o].id)
									{
										abstractor.doMagic(simulationArray.level1,simulationArray.level2[o]);
										break;
									}
							}
						}	
						else if(this.prevwhereAmI == "town" && whereAmI == "pub") //i am at pub
						{
							for(var o =0; o<simulationArray.level2.length;o++)
							{	
								//alert(simulationArray.level2[o].id+":"+simulationArray.level2.length);
								if(pubID == simulationArray.level2[o].id)						
								{
									abstractor.doMagic(simulationArray.level1,simulationArray.level2[o]);
									break;
								}
							}
						}
						else if(this.prevwhereAmI == "town" && whereAmI == "city") //i am at city
						{

							for(var o =0; o<simulationArray.level2.length;o++)
							{	
								//alert(simulationArray.level2[o].id+":"+simulationArray.level2.length);
								if(townID == simulationArray.level2[o].id)						
								{
									//alert(simulationArray.level2[o].id);
									abstractor.doMagic(simulationArray.level1,simulationArray.level2[o]);
									break;
								}
							}
						}
						else if(this.prevwhereAmI == "pub") //i am at town
						{
							for(var o =0; o<simulationArray.level2.length;o++)
							{	
								//alert(simulationArray.level2[o].id+":"+simulationArray.level2.length);
								if(townID == simulationArray.level2[o].id)						
								{
									abstractor.doMagic(simulationArray.level1,simulationArray.level2[o]);
									break;
								}
							}
						}					

						this.prevtownID = townID;
						this.prevpubID = pubID;
						this.prevwhereAmI = whereAmI;				
					}			
						//store previous room data here

						//map[player.row][player.col].occupied=0;
						robber = [];
						police = [];
						dealer = [];										
						map[x][y].occupied = 0;
						map[x][y].id = "null";
						map[x][y].keyID = 0;
						//console.log("softresetOccuring!");
					if(x==17&&y==9)	
					{
						for(var ri=0;ri<gridOccupiedArray.length;)
						{gridOccupiedArray.pop();}
						var timedifference = 9999;
			            for(var j=0;j<this.tempPeople.length;j++)
			            {
			            	timedifference = (Date.now()/1000) - this.tempPeople[j].destroyTimer;
			              if(timedifference > 5)
			              {
			              	this.tempPeople.splice(j,1);
			              	j-=1;
			              }
			            }

			            for(var j=0;j<gameboard.tempPeople.length;j++)
			            {
			            	timedifference = (Date.now()/1000) - this.tempPeople[j].destroyTimer;			            	
			            	this.tempPeople[j].leave=false;
			            	if(gameLevel === 3) //exit from 2 to 3----------------------------------------
			            	{
			            		randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
			            		randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);
			            		//alert("rr:"+randomrow+" rc:"+randomcol);
			            		for(;;)//check if it is already occupied
			            			{
			            				console.log(randomrow+":"+randomcol+" td:"+timedifference);
			            				if(map[randomrow][randomcol].occupied)
			            				{
			            					randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
			            					randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);
			            				}
			            				else
			            					{break;}
			            			}
			            		this.tempPeople[j].pos_x=map[randomrow][randomcol].pos.pos_x;
			            		this.tempPeople[j].pos_y=map[randomrow][randomcol].pos.pos_y;
	            				eval(this.tempPeople[j].id+".push(this.tempPeople["+j+"])");
	            				//alert("believe!");
			            	}
			            	if(gameLevel === 2) // entry from 3 to 2 or exit from 1 to 2-------------------------
			            	{
			            		//enter 1 of the 4 towns
			            		if(this.tempPeople[j].col == 1 || this.tempPeople[j].col == 4 || this.tempPeople[j].col == 5 || this.tempPeople[j].col == 8)
			            		{

			            			if(this.tempPeople[j].col === 1&&townID===1 || this.tempPeople[j].col === 5&&townID===2 || this.tempPeople[j].col === 4&&townID===3 || this.tempPeople[j].col === 8&&townID===4)//run only if the player enter the same town as the exiter
			            			{
	            					randomrow = Math.floor(1+Math.random()*timedifference);
	            					randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);			            			
			            		for(;;)
			            			{
			            				console.log(randomrow+":"+randomcol+" td:"+timedifference);
			            				if(map[randomrow][randomcol].occupied)
			            				{
			            					randomrow = Math.floor(1+Math.random()*timedifference);
			            					randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);
			            				}
			            				else
			            					{break;}
			            			}	
				            		this.tempPeople[j].pos_x=map[randomrow][randomcol].pos.pos_x;
				            		this.tempPeople[j].pos_y=map[randomrow][randomcol].pos.pos_y;
		            				eval(this.tempPeople[j].id+".push(this.tempPeople["+j+"])");	
		            				//alert("believe!");			            			
			            			}		            			
	            		
			            		}
			            		else//exit from one of the 4 pubs
			            		{
			            		randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
			            		randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);
			            		//alert("rr:"+randomrow+" rc:"+randomcol);
			            		for(;;)//check if it is already occupied
			            			{
			            				console.log(randomrow+":"+randomcol+" td:"+timedifference);
			            				if(map[randomrow][randomcol].occupied)
			            				{
			            					randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
			            					randomcol = Math.floor(((this.columns/2)-2.5)+Math.random()*timedifference);
			            				}
			            				else
			            					{break;}
			            			}
			            		this.tempPeople[j].pos_x=map[randomrow][randomcol].pos.pos_x;
			            		this.tempPeople[j].pos_y=map[randomrow][randomcol].pos.pos_y;
	            				eval(this.tempPeople[j].id+".push(this.tempPeople["+j+"])");
	            				//alert("believe!");
			            		}
			            	}
			            	else //enter 1 of the 4 pubs
			            	{
			            		if(pubID === 1 && (this.tempPeople[j].row === 5&&this.tempPeople[j].col == 2) || pubID === 2 && (this.tempPeople[j].row === 5&&this.tempPeople[j].col == 7) || pubID === 3 && (this.tempPeople[j].row === 12&&this.tempPeople[j].col == 2) || pubID === 4 && (this.tempPeople[j].row === 12&&this.tempPeople[j].col == 7) )//player enter the same pub as exiter
			            		{
			            		randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
			            		randomcol = Math.floor(2+Math.random()*timedifference);	
			            		for(;;)
			            			{
			            				console.log(randomrow+":"+randomcol+" td:"+timedifference);
			            				if(map[randomrow][randomcol].occupied)
			            				{
						            		randomrow = Math.floor(((this.rows/2)-2.5)+Math.random()*timedifference);
						            		randomcol = Math.floor(2+Math.random()*timedifference);	
			            				}
			            				else
			            					{break;}
			            			}	
				            		this.tempPeople[j].pos_x=map[randomrow][randomcol].pos.pos_x;
				            		this.tempPeople[j].pos_y=map[randomrow][randomcol].pos.pos_y;
		            				eval(this.tempPeople[j].id+".push(this.tempPeople["+j+"])");	
		            				//alert("believe!");			            		
			            		}

			            	}

			            }	
						this.tempPeople=[];				            
			            for(var j=0;j<robber.length;j++)
			            {
			              robber[j].keyID=j;
			            }  		
			            for(var j=0;j<police.length;j++)
			            {
			              police[j].keyID=j;
			            }  					            	            		            
			            for(var j=0;j<dealer.length;j++)
			            {
			              dealer[j].keyID=j;
			            } 
			            this.spawnrate=0; 					            		
					}						
					}	
					
		
				}
			}	
			//console.log("-------------");	
		}
		this.softreset = 1;
		this.initMapFlag = 1;
	}
	
	// Draw FPS
	this.debugdrawFPS = function(){
		gameEnd = Date.now();
		gameTime = gameEnd - gameStart;
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = '#FFFFFF'; 
		db.font = "normal 14px Verdana";		
		db.fillText("FPS: " +gameTime, 1*debug.width/20, 1*debug.height/20);	
		db.closePath();
		gameStart = gameEnd;
	}
	
	// Draw Grid
	this.debugdrawGrid = function(){
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = '#FFFFFF'; 
		db.font = "normal 14px Verdana";		
		db.fillText("Grid Size: " +gridSize+ "px", 1*debug.width/20, 2*debug.height/20);	
		db.closePath();		
	}
	
	// Draw Parameters for different parameters in the entire code
	this.debugdrawParameters = function(){		
		// Output the town ID
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = 'white'; 
		db.font = "normal 14px Verdana";		
		db.fillText("Town: " + townID, 1*debug.width/20, 2*debug.height/20);	
		db.closePath();		
		
		// Output the pub ID
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = 'white'; 
		db.font = "normal 14px Verdana";		
		db.fillText("Pub: " + pubID, 1*debug.width/20, 3*debug.height/20);	
		db.closePath();
		
		// Output the Simulation Level 2
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = 'red'; 
		db.font = "normal 14px Verdana";		
		db.fillText("LEVEL 2 ", 1*debug.width/20, 4*debug.height/20);	
		db.closePath();
		
		// Output the Simulation Level 3
		db.beginPath();
		db.strokeStyle = "#FFFFFF";
		db.fillStyle   = 'blue'; 
		db.font = "normal 14px Verdana";		
		db.fillText("LEVEL 3 ", 1*debug.width/20, 10*debug.height/20);	
		db.closePath();

		if(simulationInit > 2){
			// Level 2
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText("P:R:D: " + Math.floor(simulationArray.level2[0].character.police) + ":" + Math.floor(simulationArray.level2[0].character.robber) + ":" + Math.floor(simulationArray.level2[0].character.dealer), 1*debug.width/20, 5*debug.height/20);	
			db.closePath();
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText("cA:cB:cC:cD", 1*debug.width/20, 6*debug.height/20);	
			db.closePath();			
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText(Math.floor(simulationArray.level2[0].candy.candy1) + ":" + Math.floor(simulationArray.level2[0].candy.candy2) + ":" + Math.floor(simulationArray.level2[0].candy.candy3) + ":" + Math.floor(simulationArray.level2[0].candy.candy4), 1*debug.width/20, 7*debug.height/20);	
			db.closePath();
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText("rA:rB:rC:rD", 1*debug.width/20, 8*debug.height/20);	
			db.closePath();
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText(Math.floor(simulationArray.level2[0].rumour.rumourA) + ":" + Math.floor(simulationArray.level2[0].rumour.rumourB) + ":" + Math.floor(simulationArray.level2[0].rumour.rumourC) + ":" + Math.floor(simulationArray.level2[0].rumour.rumourD), 1*debug.width/20, 9*debug.height/20);	
			db.closePath();
			
			// Level 3
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText("pCandy: " + Math.floor(simulationArray.level3[0].promCandy), 1*debug.width/20, 11*debug.height/20);	
			db.closePath();		

			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = 'white'; 
			db.font = "normal 14px Verdana";		
			db.fillText("pRumour: " + Math.floor(simulationArray.level3[0].promRumour), 1*debug.width/20, 12*debug.height/20);	
			db.closePath();		
			
		}
	}
    this.spawnEnemies = function(maxNum,identity)
    {     

    	var timenow = Date.now()/1000;
        //randomizer to check for occupied space
        var spawnRow = 0;
        var spawnCol = 0;  
        
        if(robber.length == 0)
        {this.spawnrate = 0;}
//       	if(dealer.length == maxNum)
//        {this.spawnrate = (Math.random()*10)+4.5;}

        
        //console.log(timenow-this.prevSpawnTime);
        if((timenow-this.prevSpawnTime > this.spawnrate))
        {
        //console.log(this.spawnrate);
        if(gameLevel == 1)
        {
        	//78
            spawnRow = 7;
            spawnCol = 8;        
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 8;
	            spawnCol = 8;            	
            }                 	
        }
        else if(gameLevel == 2)
        {
        	//14; 43; 11 3; 11 8; 48;
        	var ranindx = Math.floor(Math.random()*(this.townspawnarea.length-1));
            spawnRow = this.townspawnarea[ranindx].row;
            spawnCol = this.townspawnarea[ranindx].col;
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 4;
	            spawnCol = 3;            	
            }       
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 11;
	            spawnCol = 3;            	
            }   
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 11;
	            spawnCol = 8;            	
            }               
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 4;
	            spawnCol = 8;            	
            }                                               	
        }
    	else
    	{
    		//36; 72; 11 5; 99
        	var ranindx = Math.floor(Math.random()*(this.cityspawnarea.length-1));
            spawnRow = this.cityspawnarea[ranindx].row;
            spawnCol = this.cityspawnarea[ranindx].col;    		

			if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 7;
	            spawnCol = 2;            	
            }              
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 7;
	            spawnCol = 2;            	
            }       
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 11;
	            spawnCol = 5;            	
            }   
            if(map[spawnRow][spawnCol].occupied)
            {
	            spawnRow = 9;
	            spawnCol = 9;            	
            }                   		
    	}

                

        while(map[spawnRow][spawnCol].occupied)
        {
            spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
            spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1);   
        }
       // map[spawnRow][spawnCol].occupied = 1;

        if(identity == 'robber')
        {
         //   console.log("ant length:" + ants.length + " : "+maxNum);
            if(robber.length < maxNum)
                {    
                	//console.log(spawnRow+":1st"+spawnCol);
                    robber.push(new RobberObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"robber"));
                	this.prevSpawnTime = timenow;
                	if(robber.length!=0)
                	{robber[robber.length-1].keyID = robber.length-1;}
                	if(simulationInit > 1)
                	{
                	if(robber.length!==0)
                	{
                	//console.log("Real Robber Length:"+robber.length);	
                	//console.log("Sim Robber Length:"+simulationArray.level1.robber.length);
                	robber[robber.length-1].stealType = simulationArray.level1.robber[robber.length-1].stealType;
                	robber[robber.length-1].stealAmount = simulationArray.level1.robber[robber.length-1].stealAmount;
                	}

              		}
                }
                else if (robber.length > maxNum)//1 random robber will leave the pub
                {
                	robber[Math.floor(Math.random()*(robber.length-1))].leave=true;

                	this.prevSpawnTime = timenow;
                }            
        }
        else if(identity == 'police')
        {
            if(police.length < maxNum)
                {
                    police.push(new PoliceObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"police"));
                	this.prevSpawnTime = timenow;
                	if(police.length!=0)
                	{police[police.length-1].keyID = police.length-1; }  
                	if(simulationInit > 1)
                	{
                	if(police.length!==0)
                	{
                	//	console.log("Police Length:"+simulationArray.level1.police.length);
                	//	alert("Robber Length:"+simulationArray.level1.police.length);
                	police[police.length-1].bribeAmount = simulationArray.level1.police[police.length-1].bribeAmount;
                	}

              		}                	             	
                }    
                else if (police.length > maxNum)//1 random robber will leave the pub
                {
                	police[Math.floor(Math.random()*(police.length-1))].leave=true;

                	this.prevSpawnTime = timenow;
                }                 
        }
        else if(dealer.length < maxNum)
            {
            	dealer.push(new DealerObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"dealer"));
            	this.prevSpawnTime = timenow;
            	if(dealer.length!=0)
                {dealer[dealer.length-1].keyID = dealer.length-1; } 
                	if(simulationInit > 1)
                	{
                	if(dealer.length!==0)
                	{
                	//console.log("Real Dealer Length:"+dealer.length);
                	//console.log("Sim Dealer Length:"+simulationArray.level1.dealer.length);
                	//	alert("Robber Length:"+simulationArray.level1.dealer.length);
                	dealer[dealer.length-1].rumourAcount = simulationArray.level1.dealer[dealer.length-1].rumourAcount;
                	dealer[dealer.length-1].rumourBcount = simulationArray.level1.dealer[dealer.length-1].rumourBcount;
                	dealer[dealer.length-1].rumourCcount = simulationArray.level1.dealer[dealer.length-1].rumourCcount;
                	dealer[dealer.length-1].rumourDcount = simulationArray.level1.dealer[dealer.length-1].rumourDcount;
                	dealer[dealer.length-1].rumourThres = simulationArray.level1.dealer[dealer.length-1].rumourThres;
                	dealer[dealer.length-1].rumourProb = simulationArray.level1.dealer[dealer.length-1].rumourProb;
                	dealer[dealer.length-1].candy1Price = simulationArray.level1.dealer[dealer.length-1].candy1Price;
                	dealer[dealer.length-1].candy2Price = simulationArray.level1.dealer[dealer.length-1].candy2Price;  
                	dealer[dealer.length-1].candy3Price = simulationArray.level1.dealer[dealer.length-1].candy3Price;
                	dealer[dealer.length-1].candy4Price = simulationArray.level1.dealer[dealer.length-1].candy4Price;    
                	dealer[dealer.length-1].candy1 = simulationArray.level1.dealer[dealer.length-1].candy1;
                	dealer[dealer.length-1].candy2 = simulationArray.level1.dealer[dealer.length-1].candy2;  
                	dealer[dealer.length-1].candy3 = simulationArray.level1.dealer[dealer.length-1].candy3;
                	dealer[dealer.length-1].candy4 = simulationArray.level1.dealer[dealer.length-1].candy4;                  	              	              	

                	}

              		}                  
                if(dealer.length === maxNum)
                {
                	this.mapSpawnFinish=true;
                	if(simulationInit!==3)
                	{simulationInit = 1;}
                }        	
            }
            else if (dealer.length > maxNum)//1 random robber will leave the pub
            {
            	dealer[Math.floor(Math.random()*(dealer.length-1))].leave=true;

            	this.prevSpawnTime = timenow;
            }             

        
   		}	
	}
}
