//Tey Jun Hong U095074X
//Arison Neo A0077784L
// ------------------------------------------------------------------
// Door prototype for city and region
// ------------------------------------------------------------------
var DoorObj = function(){	
	this.posArray = new Array();
	this.initFlag = 0;
	this.town = new Image();
	this.town.src = "asset/image/town.png";
	this.house = new Image();
	this.house.src = "asset/image/house.png";
	this.tree = new Image();
	this.tree.src = "asset/image/tree.png";
	return this;
};

DoorObj.prototype = {	
	setLocation : function(){
		if(this.initFlag == 0){
			if(gameLevel == 1){
				this.posArray = [];	
				var pos = {
					x : 7,
					y : 9,
					grid : 80
				};
				this.posArray.push(pos);
				var pos = {
					x : 8,
					y : 9,
					grid : 90
				};
				this.posArray.push(pos);
				
				// Empty the array
				doorOccupiedArray = [];		
				for(var i=0;i<this.posArray.length;i++){
					// Need to convert this to gridArray
					doorOccupiedArray.push(this.posArray[i]);
				}
			}else if(gameLevel == 2){
				// Level 2
				this.posArray = [];	
				var pos = {
					x : 0,
					y : 4,
					grid : 5
				};
				this.posArray.push(pos);
				var pos = {
					x : 0,
					y : 5,
					grid : 6
				};
				this.posArray.push(pos);
				var pos = {
					x : 5,
					y : 2,
					grid : 53,
					num : 1
				};
				this.posArray.push(pos);
				var pos = {
					x : 5,
					y : 7,
					grid : 58,
					num : 2
				};
				this.posArray.push(pos);
				var pos = {
					x : 12,
					y : 2,
					grid : 123,
					num : 3
				};
				this.posArray.push(pos);
				var pos = {
					x : 12,
					y : 7,
					grid : 128,
					num : 4
				};
				this.posArray.push(pos);
				
				// Empty the array
				doorOccupiedArray = [];			
				for(var i=0;i<this.posArray.length;i++){
					// Need to convert this to gridArray
					doorOccupiedArray.push(this.posArray[i]);
				}
			}else if(gameLevel == 3){
				// Level 3	
				this.posArray = [];	
				var pos = {
					x : 8,
					y : 1,
					grid : 82,
					num : 1
				};
				this.posArray.push(pos);
				var pos = {
					x : 4,
					y : 5,
					grid : 46,
					num : 2
				};
				this.posArray.push(pos);
				var pos = {
					x : 12,
					y : 4,
					grid : 125,
					num : 3				
				};
				this.posArray.push(pos);
				var pos = {
					x : 9,
					y : 8,
					grid : 99,
					num : 4
				};
				this.posArray.push(pos);
				
				// Empty the array
				doorOccupiedArray = [];			
				for(var i=0;i<this.posArray.length;i++){
					// Need to convert this to gridArray
					doorOccupiedArray.push(this.posArray[i]);
				}
			}	
			this.initFlag = 1;
		}
	},
	
	// Draw circle for now
	draw : function(){
		for(var x = 0; x < 18; x++){
			for(var y = 0; y < 10; y++){
				for(var i=0;i<doorOccupiedArray.length;i++){
					if(doorOccupiedArray[i].x == x && doorOccupiedArray[i].y == y){	
						if(gameLevel == 3){
							ctx.beginPath();
							ctx.save();
							ctx.drawImage(this.town, x*50,y*50);
							ctx.restore();
							ctx.closePath();
						}
						if(gameLevel == 2){
							if(x > 2){	// House
								ctx.beginPath();
								ctx.save();
								ctx.drawImage(this.house, x*50,y*50);
								ctx.restore();
								ctx.closePath();
							}else{
								ctx.beginPath();
								ctx.save();
								ctx.drawImage(this.tree, x*50,y*50);
								ctx.restore();
								ctx.closePath();							
							}
						}
						if(gameLevel == 1){
							/*
							ctx.beginPath();
							ctx.fillStyle="#FFD39B";
							ctx.rect(x*50,y*50,50,50);
							ctx.fill();
							ctx.lineWidth = 4;
							ctx.strokeStyle = '#733D1A';				
							ctx.stroke();
							ctx.closePath();
							*/
						}
					}
				}
			}
		}	
	}
}
