//Tey Jun Hong U095074X
//Arison Neo A0077784L
//Muhammad Azri A0073951B 

// ------------------------------------------------------------------
// Player Prototype
// ------------------------------------------------------------------
var PlayerObj = function(pos_x, pos_y, radius, id){
	// Set Parameters of players
	this.pos_x = pos_x;
	this.pos_y = pos_y;
	this.radius = radius;
	this.grid = 1;			// ID of grid
	this.moveTimer = 0;
	this.state = 0;			// State of player: 0: movable, 1: stay
	this.id = id;
	this.orientation = 4; 	// 1: Up, 2: Left, 3: Down, 4: Right 
	this.stop = 0;			// Stop the player to go 1: Up,	2: Left, 3: Down, 4: Right 	
	this.row = 0;
	this.col = 0;	
	this.candy1 = 10;		// Quantity of candies
	this.candy2 = 20;
	this.candy3 = 30;
	this.candy4 = 40;
	this.rumourAcount = 0;	// Number of times the person is in contact with rumour A
	this.rumourBcount = 0;
	this.rumourCcount = 0;
	this.rumourDcount = 0;	
	this.rumourHeld="No Rumor";
	return this;		
};
	
PlayerObj.prototype = {	
	// Get the max rumor
	whichRumor : function(){
		var maxRumor = Math.max(this.rumourAcount,this.rumourBcount,this.rumourCcount,this.rumourDcount);		
		if(maxRumor == this.rumourAcount)
			{this.rumourHeld="rumourAcount";}
		if(maxRumor == this.rumourBcount)
			{this.rumourHeld="rumourBcount";}
		if(maxRumor == this.rumourCcount)
			{this.rumourHeld="rumourCcount";}
		if(maxRumor == this.rumourDcount)
			{this.rumourHeld="rumourDcount";}	
			return this.rumourHeld;							
	},		
	// Draw circle for now
	draw : function(){
      	ctx.beginPath();
		ctx.arc(this.pos_x, this.pos_y, this.radius, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'green';
		ctx.fill();
		ctx.strokeStyle = '#003300';
		ctx.stroke();
      	ctx.closePath();
      	
		// The Face
		ctx.strokeStyle = '#0000FF';
		ctx.fillStyle = '#FFFF00';
		ctx.lineWidth = 4;
		ctx.beginPath();
		ctx.arc(this.pos_x,this.pos_y,this.radius,0*Math.PI,2*Math.PI,true);
		ctx.closePath();
		ctx.stroke();
		ctx.fill();
	 
	 	// Facing up
	 	if(this.orientation == 1){
			// The Left eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x-10,this.pos_y-10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
	 
			// The Right Eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x+10,this.pos_y-10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	 	// Facing Left
	 	if(this.orientation == 2){
			// The Left eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x-10,this.pos_y-10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
	 
			// The Right Eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x-10,this.pos_y+10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	 	// Facing Down
	 	if(this.orientation == 3){
			// The Left eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x-10,this.pos_y+10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
	 
			// The Right Eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x+10,this.pos_y+10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	 	// Facing Down
	 	if(this.orientation == 4){
			// The Left eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x+10,this.pos_y-10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
	 
			// The Right Eye
			ctx.strokeStyle = '#000000';
			ctx.fillStyle = '#000000';
			ctx.beginPath();
			ctx.arc(this.pos_x+10,this.pos_y+10,2,0*Math.PI,2*Math.PI,false);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	},
	
	// Move the player
	move : function(){	
		if(this.state == 0){
			this.moveTimer++;
			if(this.moveTimer % 5 == 0){
				this.row = pathfinding.getObjectIndexRow(player);
				this.col = pathfinding.getObjectIndexCol(player);					
				var move = '';
				if(gameboard.softreset !== 0 && gameboard.mapSpawnFinish)
				{
				if(38 in keysDown){ // Player holding up
					if(this.pos_y > gridSize  && this.stop != 1){ 
						pathfinding.objectgo(player,map[this.row][this.col-1].pos);
					}
					move += 'up ';
					this.orientation = 1;
				}
		
				if(37 in keysDown){ // Player holding left
					if(this.pos_x > gridSize && this.stop != 2){ 
						pathfinding.objectgo(player,map[this.row-1][this.col].pos);
					}
					move += 'left ';
					this.orientation = 2;
				}
		
				if(39 in keysDown){ // Player holding right
					if(this.pos_x < canvas.width-gridSize && this.stop != 4){ 
						pathfinding.objectgo(player,map[this.row+1][this.col].pos);
					}
					move += 'right ';
					this.orientation = 4;
				}
		
				if(40 in keysDown){ // Player holding down
					if(this.pos_y < canvas.height-gridSize && this.stop != 3){ 
						pathfinding.objectgo(player,map[this.row][this.col+1].pos);
					}
					move += 'down ';
					this.orientation = 3;
				}		
				db.beginPath();
				db.strokeStyle = "#FFFFFF";
				db.fillStyle   = '#FFFFFF'; 
				db.font = "normal 14px Verdana";		
				db.fillText(move, 12*debug.width/20, 3*debug.height/20);	
				db.closePath();
				this.checkPos();
				this.pushGridPos();
			}
			}
		}
	},
	
	// A check on the player's grid collision based on the position he is on
	// Diagonal vertices can actually be removed now as it is not needed
	checkPos : function(){
		// NSEW vertices for player
		var N = math.vector(this.pos_x, this.pos_y - this.radius);
		var S = math.vector(this.pos_x, this.pos_y + this.radius);
		var E = math.vector(this.pos_x + this.radius, this.pos_y);
		var W = math.vector(this.pos_x - this.radius, this.pos_y);
		var NE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));
		var NW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y - this.radius/Math.sqrt(2));		
		var SE = math.vector(this.pos_x + this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));
		var SW = math.vector(this.pos_x - this.radius/Math.sqrt(2), this.pos_y + this.radius/Math.sqrt(2));		
		var Center = math.vector(this.pos_x, this.pos_y);
		
		// Vertices collision flag
		var Npos = math.getGridPos(N.x, N.y);
		var Spos = math.getGridPos(S.x, S.y);
		var Epos = math.getGridPos(E.x, E.y);
		var Wpos = math.getGridPos(W.x, W.y);
		var NEpos = math.getGridPos(NE.x, NE.y);
		var NWpos = math.getGridPos(NW.x, NW.y);
		var SEpos = math.getGridPos(SE.x, SE.y);
		var SWpos = math.getGridPos(SW.x, SW.y);
		var Centerpos = math.getGridPos(Center.x, Center.y);
		
		// Check the grid it collide with and store in posArray
		var posArray = new Array();		
		posArray = collision.checkPlayerGridCollision(Npos,Spos,Epos,Wpos,NEpos,NWpos,SEpos,SWpos,Centerpos);	
		for(var i=0;i<posArray.length;i++){
			db.beginPath();
			db.strokeStyle = "#FFFFFF";
			db.fillStyle   = '#FFFFFF'; 
			db.font = "normal 14px Verdana";		
			db.fillText(posArray[i], 12+i*debug.width/5, 5*debug.height/20);	
			db.closePath();					
		}	
	},
	
	// Push grid pos to a global array so that the controller has a bird eye view
	pushGridPos : function(){
		var tempgrid = math.getGridPos(this.pos_x, this.pos_y)
		// Splice the previous grid out from gridOccupiedArray
			for(var i=0;i<gridOccupiedArray.length;i++){
				if(gridOccupiedArray[i].grid == this.grid){
					gridOccupiedArray.splice(i, 1);						
				}else{
				
				}
			}			
		this.grid = tempgrid;
		// Then push new grid into gridOccupiedArray
		gridOccupiedArray.push({id : this.id , grid : this.grid});	
		//console.log(gridOccupiedArray);
		
		// Store new position inside map
  		var row = Math.floor(this.pos_x/gridSize);
  		var col = Math.floor(this.pos_y/gridSize);
		map[row][col].occupied = 1;
		map[row][col].id = this.id;
//		console.log(row+":"+col);	
	},
	
	// Inventory of player with a summary of sweet
	inventory : function(){
		if(this.state == 0){	// Player movable	
			if(16 in keysDown){
				ctx.beginPath();
				ctx.fillStyle="white";
				ctx.rect(5,canvas.height-100,canvas.width-10,90);
				ctx.fill();
				ctx.lineWidth = 4;
				ctx.strokeStyle = 'red';				
				ctx.stroke();
				ctx.closePath();
			
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("     CANDY 1     x      " + this.candy1, 20, canvas.height-60);	
				ctx.closePath();

				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("     CANDY 2     x      " + this.candy2, 20, canvas.height-35);	
				ctx.closePath();
				
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("CANDY 3     x      " + this.candy3, 450, canvas.height-60);	
				ctx.closePath();
				
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'black'; 
				ctx.font = "normal 25px Verdana";		
				ctx.fillText("CANDY 4     x      " + this.candy4, 450, canvas.height-35);	
				ctx.closePath();			

				var hotrumour = player.whichRumor();
				if(hotrumour == "rumourAcount")
					{hotrumour="Candy A?";}
				if(hotrumour == "rumourBcount")
					{hotrumour="Candy B?";}
				if(hotrumour == "rumourCcount")
					{hotrumour="Candy C?";}
				if(hotrumour == "rumourDcount")
					{hotrumour="Candy D?";}
				ctx.beginPath();
				ctx.strokeStyle = "#000";
				ctx.fillStyle   = 'blue'; 
				ctx.font = "normal 20px Arial";		
				ctx.fillText("What I think is HOT:"+hotrumour, 280, canvas.height-15);	
				ctx.closePath();					
			}
		}		
	}
};