//Tey Jun Hong U095074X
//Saikrishnan Ranganathan U096089W
// ------------------------------------------------------------------
// Request Animate Frame
// ------------------------------------------------------------------
window.requestAnimFrame = (function(){
    return window.requestAnimationFrame  ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame  ||
        window.msRequestAnimationFrame  ||
        function(/* function */ callback, /* DOMElement */ element){
            window.setTimeout(callback, 1000 / 60);
        };
})();

// ------------------------------------------------------------------
// Load the game when html body is loaded
// ------------------------------------------------------------------
function load(){
	// Initialise any global variable
	initGlobal();

	// Initialise game
	initGame();

	// Time that game start
	gameStart = Date.now();

	// Start updating 
	update();
};

// -------------------------------------------------------------------
// Globals - Variables that Objects keep using
// -------------------------------------------------------------------
function initGlobal(){
	// Player parameter
	playerHealth = 10;	// 10 health at the start
	amount = 10000; 	// 200 dollars at the start

	// Map / Grid System
	gridArray = new Array();
	gridSize = 50;
	gridOccupiedArray = new Array(); // Tell the system which grid is already occupied
	map = new Array(); // For map[gridX][gridY]
	
	// Level System
	gameLevel = 3; // define game board level // level 1: pub, level 2: town, level 3: city
	doorOccupiedArray = new Array();	
	townID = 0;
	pubID = 0;
	bgInitFlag = 0;

	// Key Control
	keysDown = {};
	keysUp = {};
	
	// Simulation
	simulationArray = new Array();
	simulationInit = 0;
	simulationFlag = 0;
}

// ------------------------------------------------------------------
// Initialise Game Backend and Setting
// ------------------------------------------------------------------
function initGame(){
    // Game 
    canvas = document.getElementById("game");
    ctx = canvas.getContext("2d");
    // Right Debug
    debug = document.getElementById("debug");
    db = debug.getContext("2d");
    // Debug for Game
    debug2 = document.getElementById("game");
    cdb = debug.getContext("2d");

    // Listen for key set
    addEventListener("keydown", function (e){
        e.preventDefault();
        delete keysUp[e.keyCode];
        keysDown[e.keyCode] = true;
    }, false);
    // Listen for key unset
    addEventListener("keyup", function (e){
        e.preventDefault();
        delete keysDown[e.keyCode];
        keysUp[e.keyCode] = true;
    }, false);
    
    // Init Objects
    gameboard = new gameBoard();
    player = new PlayerObj(425,275,20,"player");
    robber = new Array();//RobberObj(275,275,20,"robber");
    police = new Array();//PoliceObj(275,375,20,"police");
    dealer = new Array();//DealerObj(275,175,20,"dealer");

//    robber.push(new RobberObj(275,275,20,"robber"));
//    police.push(new PoliceObj(275,375,20,"police"));
//    dealer.push(new DealerObj(275,175,20,"dealer"));

    door = new DoorObj();

    // Init Helpers
    collision = new collisionHelper();
    math = new mathHelper();
    pathfinding = new Pathfinding();
}

// ------------------------------------------------------------------
// Reset the game state
// ------------------------------------------------------------------
function reset(type){
}

// ------------------------------------------------------------------
// Update the objects at frame rate
// ------------------------------------------------------------------
function update(){
	gameboard.initMap();
	gameboard.showGrid();

	door.setLocation();
	collide();
	



    if(simulationInit < 3)
    {
        if(gameLevel==2)
        {alert("oh no!");}
        gameboard.spawnEnemies(4,"robber");
        gameboard.spawnEnemies(4,"police");
        gameboard.spawnEnemies(20,"dealer");  
         
    }
    else 
    {
       // console.log(simulationArray.level1.robber.length);
        gameboard.spawnEnemies(simulationArray.level1.robber.length,"robber");
        gameboard.spawnEnemies(simulationArray.level1.police.length,"police");
        gameboard.spawnEnemies(simulationArray.level1.dealer.length,"dealer");        
    }
    
	// Initialise / Run the simulation
	simulate();
	if(simulationInit > 2){
		abstractSimulator.simulate();
	}
    for(var k=0;k<robber.length;k++){
    	robber[k].move();
    }
    for(var k=0;k<police.length;k++){
    	police[k].move();
    }
    for(var k=0;k<dealer.length;k++){
    	dealer[k].move();
    }
    player.move();

	animate();
}

// ------------------------------------------------------------------
// Initialise / Run the simulation
// ------------------------------------------------------------------
function simulate(){	
	// Initialise the simulation
	if(simulationInit === 1 && simulationFlag === 0){
		// Level 1
		for(var k=0;k<robber.length;k++){
			// Initialise the parameters
			robber[k].stealAmount = 5;
			robber[k].stealType = k+1;		
		}
		for(var k=0;k<police.length;k++){
			police[k].bribeAmount = 100;			
		}
		for(var k=0;k<dealer.length;k++){
				dealer[k].candy1 = 10;	
				dealer[k].candy2 = 10;	
				dealer[k].candy3 = 10;	
				dealer[k].candy4 = 10;	
			if(k < 5){
				dealer[k].rumourAcount = 1;	
				dealer[k].candy1 = 50;		
			}
			if(k > 4 && k < 10){
				dealer[k].rumourBcount = 1;
				dealer[k].candy2 = 50;			
			}
			if(k > 9 && k < 15){
				dealer[k].rumourCcount = 1;
				dealer[k].candy3 = 50;			
			}
			if(k > 14 && k < 20){
				dealer[k].rumourDcount = 1;	
				dealer[k].candy4 = 50;		
			}
			dealer[k].rumourThres = 10;
            dealer[k].rumourProb = 0.5;
			dealer[k].candy1Price = 50;	
			dealer[k].candy2Price = 50;	
			dealer[k].candy3Price = 50;	
			dealer[k].candy4Price = 50;	
		}	
		
		// Level 2
		characterInfo = {
			dealer : 5,
			police : 1,
			robber : 1
		};
	
		candyInfo1 = {
			candy1 : 1,
			candy2 : 0.2,
			candy3 : 0.2,
			candy4 : 0.2		
		};
	
		rumourInfo1 = {
			rumourA : 1,
			rumourB : 0.2,
			rumourC	: 0.2,
			rumourD	: 0.2
		};
		
		candyInfo2 = {
			candy1 : 0.2,
			candy2 : 1,
			candy3 : 0.2,
			candy4 : 0.2		
		};
	
		rumourInfo2 = {
			rumourA : 0.2,
			rumourB : 1,
			rumourC	: 0.2,
			rumourD	: 0.2
		};
		
		candyInfo3 = {
			candy1 : 0.2,
			candy2 : 0.2,
			candy3 : 1,
			candy4 : 0.2		
		};
	
		rumourInfo3 = {
			rumourA : 0.2,
			rumourB : 0.2,
			rumourC	: 1,
			rumourD	: 0.2
		};
		
		candyInfo4 = {
			candy1 : 0.2,
			candy2 : 0.2,
			candy3 : 0.2,
			candy4 : 1		
		};
	
		rumourInfo4 = {
			rumourA : 0.2,
			rumourB : 0.2,
			rumourC	: 0.2,
			rumourD	: 1
		};
		
		level2Info = new Array();
		
		town1Info = {	
			id : 1,
			type : "town",
			character : characterInfo,
			candy : candyInfo1,
			rumour : rumourInfo1
		};
		level2Info.push(town1Info);
		
		town2Info = {	
			id : 2,
			type : "town",
			character : characterInfo,
			candy : candyInfo2,
			rumour : rumourInfo2
		};
		level2Info.push(town2Info);

		town3Info = {	
			id : 3,
			type : "town",
			character : characterInfo,
			candy : candyInfo2,
			rumour : rumourInfo2
		};
		level2Info.push(town3Info);
		
		town4Info = {	
			id : 4,
			type : "town",
			character : characterInfo,
			candy : candyInfo2,
			rumour : rumourInfo2
		};
		level2Info.push(town4Info);
		
		level3Info = new Array();
		
		for(var i = 0; i < 4; i++){
			for(var j = 0; j < 4; j++){	
				var pubToPush = {
					parentTownId: i+1,	
					id: j+1,
					type: "pub",
					promCandy: j+1,
					promRumour: j+1
				};
				level3Info.push(pubToPush);
			}
		}

		simulationInit = 2;
		simulationFlag = 1;
	}else if(simulationInit === 2){	
		//console.log("ENTER SIM");
		//	Clear up the array
		dealerInfoArray = new Array();
		robberInfoArray = new Array();
		policeInfoArray = new Array();	
		
		// Update the arrays
		for(var k=0;k<robber.length;k++){
			robberInfoArray.push(robber[k].getSimulationData());
		}
		for(var k=0;k<police.length;k++){
			policeInfoArray.push(police[k].getSimulationData());
		}
		for(var k=0;k<dealer.length;k++){
			dealerInfoArray.push(dealer[k].getSimulationData());	
		}
		level1Info = {
			id : 0,
            parentTownId : 0,
			type : "city",
			dealer : dealerInfoArray,
			police : policeInfoArray,
			robber : robberInfoArray
		};	
		simulationArray = {
			level1 : level1Info,
			level2 : level2Info,
			level3 : level3Info
		};
        simulationInit = 3;
	}
    
}

// ------------------------------------------------------------------
// Check for collision before rendering
// ------------------------------------------------------------------
function collide(){
	collision.checkPlayerDoorCollision(player.grid);
	collision.checkPlayerObjCollision(player.grid);
}

// ------------------------------------------------------------------
// Animate objects
// ------------------------------------------------------------------
function animate(){
	if(gameLevel == 3){
		if(bgInitFlag == 0){
			document.getElementById('gameArea').style.backgroundImage = 'url(asset/image/level3.jpg)';
			bgInitFlag = 1;
		}
	}
	if(gameLevel == 2){
		if(bgInitFlag == 0){
			document.getElementById('gameArea').style.background = '#0e9231';
			bgInitFlag = 1;
		}
	}
	if(gameLevel == 1){
		if(bgInitFlag == 0){
			document.getElementById('gameArea').style.background = '#000';
			document.getElementById('gameArea').style.backgroundImage = 'url(asset/image/level1.jpg)';
			bgInitFlag = 1;
		}
	}
    // Clear contexts
    ctx.clearRect(0,0,canvas.width,canvas.height);

    // Draw Objects
    gameboard.draw();
    gameboard.debugDraw();        
    door.draw();
    player.draw();
    for(var k=0;k<robber.length;k++)
    {robber[k].draw();}
    for(var k=0;k<police.length;k++)
    {police[k].draw();}
    for(var k=0;k<dealer.length;k++)    
    {dealer[k].draw();}
    // Draw Dialogues
    for(var k=0;k<robber.length;k++)
    {robber[k].dialogue();}
    for(var k=0;k<police.length;k++)
    {police[k].dialogue();}
    for(var k=0;k<dealer.length;k++)   
    {dealer[k].dialogue();}
	player.inventory();

	// Weather
	gameboard.updateWeather();    
    // Keep running update
    requestAnimFrame(update);
}



