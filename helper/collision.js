//Tey Jun Hong U095074X
//Saikrishnan Ranganathan U096089W

// ------------------------------------------------------------------
// List of Collision Helpers
// ------------------------------------------------------------------
function collisionHelper(){
	// Check for player colliding and interacting with other character objects
	this.checkPlayerObjCollision = function(playerGrid){
		if(gameLevel == 1||1){
			player.stop = 0;
			for(var i=0;i<gridOccupiedArray.length;i++){	
				if(gridOccupiedArray[i].id != "player"){
					// Then check for interaction
					if(13 in keysDown){
						if(gridOccupiedArray[i].grid == (playerGrid-1) && player.orientation == 1){	//Up
							player.state = 1;
							eval(gridOccupiedArray[i].id +"["+gridOccupiedArray[i].keyID+"].state = " + 1);  
						}
						if(gridOccupiedArray[i].grid == (playerGrid-10) && player.orientation == 2){	//Left
							player.state = 1;
							eval(gridOccupiedArray[i].id +"["+gridOccupiedArray[i].keyID+"].state = " + 1); 
						}
						if(gridOccupiedArray[i].grid == (playerGrid+1) && player.orientation == 3){	//Down
							player.state = 1;
							eval(gridOccupiedArray[i].id +"["+gridOccupiedArray[i].keyID+"].state = " + 1); 
						}
						if(gridOccupiedArray[i].grid == (playerGrid+10) && player.orientation == 4){	//Right
							player.state = 1;
							eval(gridOccupiedArray[i].id +"["+gridOccupiedArray[i].keyID+"].state = " + 1);  
						}  
					}
				}		
			}	
		}
	}	

	// Check for player exiting a level map
	this.checkPlayerDoorCollision = function(playerGrid){
		if(gameLevel == 1){
			for(var i=0;i<doorOccupiedArray.length;i++){		
				if(playerGrid == doorOccupiedArray[i].grid){
					gameLevel = 2;
					door.initFlag = 0;
					bgInitFlag = 0;
					gameboard.softreset=0;
					if(pubID == 1){
						player.pos_x = 4*gridSize+gridSize/2;
						player.pos_y = 2*gridSize+gridSize/2;
					}
					if(pubID == 2){
						player.pos_x = 4*gridSize+gridSize/2;
						player.pos_y = 7*gridSize+gridSize/2;
					}
					if(pubID == 3){
						player.pos_x = 13*gridSize+gridSize/2;
						player.pos_y = 2*gridSize+gridSize/2;
					}
					if(pubID == 4){
						player.pos_x = 13*gridSize+gridSize/2;
						player.pos_y = 7*gridSize+gridSize/2;
					}
					pubID = 0;
				}
			}			
		}else if(gameLevel == 2){
			for(var i=0;i<doorOccupiedArray.length;i++){		
				if(playerGrid == doorOccupiedArray[i].grid){
					if(playerGrid < 10){
						gameLevel = 3;
						if(townID == 1){
							player.pos_x = 8*gridSize+gridSize/2;
							player.pos_y = 2*gridSize+gridSize/2;
						}
						if(townID == 2){
							player.pos_x = 5*gridSize+gridSize/2;
							player.pos_y = 5*gridSize+gridSize/2;
						}
						if(townID == 3){
							player.pos_x = 11*gridSize+gridSize/2;
							player.pos_y = 4*gridSize+gridSize/2;
						}
						if(townID == 4){
							player.pos_x = 9*gridSize+gridSize/2;
							player.pos_y = 7*gridSize+gridSize/2;
						}
						townID = 0;
						pubID = 0;

					}else{	
						gameLevel = 1;
						if(playerGrid == 53){
							pubID = 1;						
						}
						if(playerGrid == 58){
							pubID = 2;						
						}
						if(playerGrid == 123){
							pubID = 3;						
						}
						if(playerGrid == 128){
							pubID = 4;						
						}
						player.pos_x = 7*gridSize+gridSize/2;
						player.pos_y = 8*gridSize+gridSize/2;
					}
					door.initFlag = 0;
					bgInitFlag = 0;
					gameboard.softreset=0;
				}
			}			
		}else if(gameLevel == 3){
			for(var i=0;i<doorOccupiedArray.length;i++){		
				if(playerGrid == doorOccupiedArray[i].grid){
					gameLevel = 2;
					bgInitFlag = 0;
					gameboard.softreset=0;
					player.pos_x = 1*gridSize+gridSize/2;
					player.pos_y = 4*gridSize+gridSize/2;
					if(playerGrid == 82){
						townID = 1;						
					}
					if(playerGrid == 46){
						townID = 2;						
					}
					if(playerGrid == 125){
						townID = 3;						
					}
					if(playerGrid == 99){
						townID = 4;						
					}
					door.initFlag = 0;
				}
			}			
		}
	}
	
	this.checkPlayerGridCollision = function(N,S,E,W,NE,NW,SE,SW,Center){
		var posArray = new Array();
		posArray.push(N);
		posArray.push(S);		
		posArray.push(E);
		posArray.push(W);	
		posArray.push(NE);
		posArray.push(NW);		
		posArray.push(SE);
		posArray.push(SW);	
		posArray.push(Center);
		posArray = math.getUnique(posArray);
		return posArray;
	}	
	
	this.findDistance = function(x1, y1, x2, y2){
		var a = Math.abs(x1 - x2);
		var b = Math.abs(y1 - y2);
    	var c = Math.sqrt((a * a) + (b * b));
    	return c;	
	}
	
	this.relativeOrientation = function(firstObj, otherObj){
		return (Math.atan2((firstObj.pos_y - otherObj.pos_y),(firstObj.pos_x - otherObj.pos_x)))/Math.PI*180;
	}

	this.distanceFrom = function(firstObj, otherObj){
		return Math.sqrt(Math.pow(firstObj.pos_x-otherObj.pos_x,2) + Math.pow(firstObj.pos_y-otherObj.pos_y,2));
	}
}

