//Tey Jun Hong U095074X
// ------------------------------------------------------------------
// List of Math Helpers
// ------------------------------------------------------------------
function mathHelper(){
	// Get the grid position of any pair of vertices
	this.getGridPos = function(x,y){
		var gridX = Math.floor(x/gridSize);
		var gridY = Math.floor(y/gridSize);
		//var gridXcount = Math.floor(canvas.width/gridSize);
		var gridYcount = Math.floor(canvas.height/gridSize);
		return (gridX*gridYcount + gridY+1); 
	}
	
	// Change two 1D value to 2D vector
	this.vector = function(x,y){
		return{
			x : x,
			y : y
		};	
	}
	
	// Get unique objects in array
	this.getUnique = function(origArr){  
		var newArr = new Array();
		var origLen = origArr.length;
		var found;  
		for (var x = 0; x < origLen; x++){  
			found = undefined;  
			for(var y = 0; y < newArr.length; y++){  
				if(origArr[x] === newArr[y]){  
				  found = true;  
				  break;  
				}  
			}  
			if(!found){ 
				newArr.push(origArr[x]);  
			}
		}  
	   	return newArr;  
	}
}