//Saikrishnan Ranganathan U096089W
// Use this to (partially or completely)
// pre-set arguments for a function to be passed around and invoked later
function partial(func /*, 0..n args */) {
    var args = Array.prototype.slice.call(arguments, 1);
    return function() {
        var allArguments = args.concat(Array.prototype.slice.call(arguments));
        return func.apply(this, allArguments);
    };
}