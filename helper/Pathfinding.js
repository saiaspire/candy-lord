//Muhammad Azri A0073951B
function Pathfinding()
{
  
  this.objectgo = function(object,endpoint)
  {
    var skip = false;
    var goup = false;
    var godown = false;
    var goright = false;
    var goleft = false;      

    //console.log("Diag Vel = "+diagonalmovevel);
    var objectrow = this.getObjectIndexRow(object);
    var objectcol = this.getObjectIndexCol(object);  

    var endpointrow = this.getObjectIndexRow(endpoint);
    var endpointcol = this.getObjectIndexCol(endpoint);  

    for(var i = 0;i<doorOccupiedArray.length;i++)
      {
        map[doorOccupiedArray[i].x][doorOccupiedArray[i].y].occupied=1;
        if(endpointrow === doorOccupiedArray[i].x && endpointcol === doorOccupiedArray[i].y && object.leave || object.id==="player")
        {
          for(var j = 0; j<doorOccupiedArray.length;j++)
          {map[doorOccupiedArray[j].x][doorOccupiedArray[j].y].occupied=0;}
          break;
        }      
      }



var upfailed =0,downfailed=0,leftfailed=0,rightfailed=0;
//row index increment to go down
    if(objectcol < endpointcol)
    {
      if(!(map[(objectrow)][objectcol+1].occupied))         
      {godown = true;}
      else
      {
        downfailed = true;

        if(object.id === "dealer" && map[(objectrow)][objectcol+1].id === "dealer")
        {
          eval("dealer["+map[(objectrow)][objectcol+1].keyID+"]."+object.whichRumor()+"+=1");
         // alert("dealer"+object.keyID+" interacting");
        //alert(object.whichRumor());
        }
        if(object.id === "dealer" && map[(objectrow)][objectcol+1].id === "player")
        {
         // alert(object.whichRumor());
          eval("player."+object.whichRumor()+"+=1");
        }

      }
    }
//row index increment to go up    
    if(objectcol > endpointcol)
    {
      if(!(map[(objectrow)][objectcol-1].occupied))  
      {goup = true;}
    else
      {
        upfailed=true;

        if(object.id === "dealer" && map[(objectrow)][objectcol-1].id === "dealer")
        {
          eval("dealer["+map[(objectrow)][objectcol-1].keyID+"]."+object.whichRumor()+"+=1");
        //  alert(object.whichRumor());
         // alert("dealer"+object.keyID+" interacting");
        }
        if(object.id === "dealer" && map[(objectrow)][objectcol-1].id === "player")
        {
          eval("player."+object.whichRumor()+"+=1");
        // alert(object.whichRumor());
        }      

      }
    }
//col index increment to go right    
    if(objectrow < endpointrow)
    {
      if(!(map[objectrow+1][(objectcol)].occupied))  
      {
        goright = true;
//        alert((objectrow+1)+":"+objectcol+" Occ:"+map[objectrow+1][(objectcol)].occupied);

      }
    else
      {
        rightfailed=true;

        if(object.id === "dealer" && map[(objectrow+1)][objectcol].id === "dealer")
        {
          eval("dealer["+map[(objectrow+1)][objectcol].keyID+"]."+object.whichRumor()+"+=1");
         // alert(object.whichRumor());
        //alert("dealer"+object.keyID+" interacting");
        }
        if(object.id === "dealer" && map[(objectrow+1)][objectcol].id === "player")
        {
          eval("player."+object.whichRumor()+"+=1");
        //  alert(object.whichRumor());
        }      

      }
    }
//col index increment to go left   
    if(objectrow > endpointrow)
    {
    //left
      if(!(map[objectrow-1][(objectcol)].occupied))
      {goleft = true;}
    else
      {
        leftfailed=true;

        if(object.id === "dealer" && map[(objectrow-1)][objectcol].id === "dealer")
        {
          eval("dealer["+map[(objectrow-1)][objectcol].keyID+"]."+object.whichRumor()+"+=1");
        //  alert(object.whichRumor());
        // alert("dealer"+object.keyID+" interacting");
        }
        if(object.id === "dealer" && map[(objectrow-1)][objectcol].id === "player")
        {
          eval("player."+object.whichRumor()+"+=1");
        //  alert(object.whichRumor());
        }      

      }
    }
  

    //if not going any direction
    if(!goleft && !goright && !goup && !godown)
    {
      //only if more than 1 grid size apart
      if(collision.distanceFrom(object,endpoint) > (gridSize*1.5))
      {
      if(leftfailed)
      {
        
        if(objectcol > 0)  //not 1st row
        {
          //goupleft
          if(!(map[(objectrow-1)][objectcol-1].occupied)&& collision.distanceFrom(map[(objectrow-1)][objectcol-1].pos,endpoint) < collision.distanceFrom(object,endpoint))  
          {
            goup = true;
            goleft=true;
          } 
          //downleft
          else if(objectcol < (gameboard.columns-1)) //not last row
          if(!(map[(objectrow-1)][objectcol+1].occupied)&& collision.distanceFrom(map[(objectrow-1)][objectcol+1].pos,endpoint) < collision.distanceFrom(object,endpoint))         
          {
            godown = true;
            goleft=true;
          }             
        }
      }
      else if(rightfailed)
      {
        
        if(objectcol > 0)  //not 1st row
        {
          
          //goupright
          if(!(map[(objectrow+1)][objectcol-1].occupied)&& collision.distanceFrom(map[(objectrow+1)][objectcol-1].pos,endpoint) < collision.distanceFrom(object,endpoint))  
          {
            goup = true;
            goright=true;
          } 
          else if(objectcol < (gameboard.columns-1)) //not last row
          if(!(map[(objectrow+1)][objectcol+1].occupied)&& collision.distanceFrom(map[(objectrow+1)][objectcol+1].pos,endpoint) < collision.distanceFrom(object,endpoint))         
          {
            godown = true;
            goright=true;
          }             
        }
      }
      else if(upfailed)
      {
        
        if(objectrow > 0)  //not 1st col
        {
          
          //goupleft
          if(!(map[(objectrow-1)][objectcol-1].occupied)&& collision.distanceFrom(map[(objectrow-1)][objectcol-1].pos,endpoint) < collision.distanceFrom(object,endpoint))  
          {
            goleft=true;            
            goup = true;

          } 
          else if(objectrow < (gameboard.rows-1)) //not last col
          if(!(map[(objectrow+1)][objectcol-1].occupied)&& collision.distanceFrom(map[(objectrow+1)][objectcol-1].pos,endpoint) < collision.distanceFrom(object,endpoint))         
          {
            goright = true;
            goup = true;
          }             
        }    
      }
      else if(downfailed)
      {
        
        if(objectrow > 0)  //not 1st col
        {
          
          //godownleft
          if(!(map[(objectrow-1)][objectcol+1].occupied)&& collision.distanceFrom(map[(objectrow-1)][objectcol+1].pos,endpoint) < collision.distanceFrom(object,endpoint))  
          {
            goleft=true;            
            godown = true;

          } 
          else if(objectrow < (gameboard.rows-1)) //not last col
          if(!(map[(objectrow+1)][objectcol+1].occupied)&& collision.distanceFrom(map[(objectrow+1)][objectcol+1].pos,endpoint) < collision.distanceFrom(object,endpoint))         
          {
            goright = true;
            godown = true;
          }             
        }         
      }
      }
    }

//direction facing if no movement enabled
//      if(leftfailed)
//      {physicsEngine.applyForceAtAngle(object,0,Math.PI);}
//      if(rightfailed)
//      {physicsEngine.applyForceAtAngle(object,0,0);}
//      if(upfailed)
//      {physicsEngine.applyForceAtAngle(object,0,Math.PI*3/2);}
//      if(downfailed)
//      {physicsEngine.applyForceAtAngle(object,0,Math.PI/2);}


//DIAGONALS

    //downright
    if(objectrow < gameboard.rows-1 && objectcol < gameboard.columns-1)
    if(map[objectrow+1][objectcol+1].occupied && goright && godown)
    {
      if(map[objectrow+1][objectcol].occupied)
      goright = false;
      else
      godown = false;
    }

    //downleft
    if(objectrow >0 && objectcol < gameboard.columns-1)
    if(map[(objectrow-1)][objectcol+1].occupied && goleft && godown)
    {
      if(map[objectrow][objectcol+1].occupied)
      godown = false;
      else
      goleft = false;
    }    

    //upright
    if(objectcol >0 && objectrow < gameboard.rows-1)
    if(map[(objectrow+1)][objectcol-1].occupied && goright && goup)
    {
      if(map[objectrow+1][objectcol].occupied)
      goright = false;
      else
      goup = false;
    }

    //upleft
    if(objectrow >0 && objectcol >0)    
    if(map[(objectrow-1)][objectcol-1].occupied && goleft && goup)
    { 
      if(map[objectrow-1][objectcol].occupied)
      goleft = false;
      else
      goup = false;
    }       

//      console.log("Up:"+goup+" Down:"+godown+" Left:"+goleft+" Right:"+goright);  
//      console.log("prevx:"+prevx+" Current X:"+ object.Intrinsic.centerPoint.x);
//if((prevy == object.Intrinsic.centerPoint.y && prevx == object.Intrinsic.centerPoint.x))
//  {
   
  if(goright && goup)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow+=1;objectcol-=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1; 
    object.pos_x+=gridSize;
    object.pos_y-=gridSize;
//    room.map[objectrow-1][objectcol+1].occupied = true;
  }
  else if(goright && godown)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow+=1;objectcol+=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1; 
    object.pos_x+=gridSize;
    object.pos_y+=gridSize;
//    room.map[objectrow+1][objectcol+1].occupied = true;
  }
  else if(goleft && goup)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow-=1;objectcol-=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;    
    object.pos_x-=gridSize;
    object.pos_y-=gridSize;
//    room.map[objectrow-1][objectcol-1].occupied = true;
  }
  else if(goleft && godown)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow-=1;objectcol+=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;    
    object.pos_x-=gridSize;
    object.pos_y+=gridSize;    
//    room.map[objectrow+1][objectcol-1].occupied = true;
  }
  else if(goup)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectcol-=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;      
    object.pos_y-=gridSize;
//    room.map[objectrow-1][objectcol].occupied = true;
  }
  else if(godown)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectcol+=1;  
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;
    object.pos_y+=gridSize;
//    room.map[objectrow+1][objectcol].occupied = true;
  }
  else if(goleft)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow-=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;    
    object.pos_x-=gridSize;
//    room.map[objectrow][objectcol-1].occupied = true;
  }
  else if(goright)
  {
    map[objectrow][objectcol].occupied=0;
    map[objectrow][objectcol].id="null";
    map[objectrow][objectcol].keyID=0;
    objectrow+=1;
    map[objectrow][objectcol].id=object.id;
    map[objectrow][objectcol].occupied=1;    
    object.pos_x+=gridSize;
//    room.map[objectrow][objectcol+1].occupied = true;
  }

  var g = 0,timenow = Date.now()/1000;

    
      for(var i = 0; i<doorOccupiedArray.length;i++)
      {
        if(doorOccupiedArray[i].x == objectrow && doorOccupiedArray[i].y == objectcol&&object.leave&&object.id!="player")
        {
            g = eval(object.id+".indexOf(object)");
            gameboard.spawnrate = (Math.random()*5)+1.5;
            
            eval("gameboard.tempPeople.push("+object.id+"[g])");
            gameboard.tempPeople[gameboard.tempPeople.length-1].destroyTimer = timenow;
            gameboard.tempPeople[gameboard.tempPeople.length-1].row=objectrow;
            gameboard.tempPeople[gameboard.tempPeople.length-1].col=objectcol;

            var theID=0;

            for(var d = 0; d<doorOccupiedArray.length;d++)
            {
              if(objectrow === doorOccupiedArray[d].x && objectcol === doorOccupiedArray[d].y )
                {
                  theID = doorOccupiedArray[d].num;
                  break;
                }
            }

            var whereHeIsGoing = {};
            var whereAmI = "nowhere";
            if(townID === 0 && pubID === 0)
              {whereAmI = "city";}
            else if(pubID === 0)
              {whereAmI = "town";}
            else
            {whereAmI="pub";}

            if(whereAmI == "city")
            {
            whereHeIsGoing.type = "town";
            whereHeIsGoing.id = theID;
            whereHeIsGoing.parentTownId = townID;
            }
            if(whereAmI == "pub")
            {
            whereHeIsGoing.type = "town";
            whereHeIsGoing.id = townID;
            whereHeIsGoing.parentTownId = townID;
            }    

            //going city
            if(whereAmI === "town" && (doorOccupiedArray[0].x === objectrow && doorOccupiedArray[0].y === objectcol || doorOccupiedArray[1].x === objectrow && doorOccupiedArray[1].y === objectcol))   
            {
            whereHeIsGoing.type = "city";
            whereHeIsGoing.id = 0;
            whereHeIsGoing.parentTownId = townID;
            }     
            else if(whereAmI === "town")
            {
            whereHeIsGoing.type = "pub";
            whereHeIsGoing.id = theID;
            whereHeIsGoing.parentTownId = townID;              
            }

            abstractSimulator.level1PersonLeaveTrigger(object, whereHeIsGoing);

            for(var j =0;j<gridOccupiedArray.length;j++)
            {
              if(gridOccupiedArray[j].id===object.id)
                {
                  gridOccupiedArray.splice(j,1);
                  //j-=1;
                }
            }
            eval(object.id+".splice(g,1)");

            map[objectrow][objectcol].occupied=0;
            for(var j=0;j<eval(object.id+".length");j++)
            {
              eval(object.id+"[j].keyID=j");

            }      
            gameboard.prevSpawnTime=Date.now()/1000;    
              
        }
      }

//  }
//gridOccupy(object);

if(gameboard.tempPeople.length>0)
{//console.log("TempPeople"+gameboard.tempPeople.length);
 }
  }//end of objectgo function


this.getObjectIndexCol = function(object)
{
  var col = Math.floor(object.pos_y/gridSize);
  //console.log("Col:"+col);
  return col;
}
this.getObjectIndexRow = function(object)
{
  var row = Math.floor(object.pos_x/gridSize);
  //console.log("Row:"+row);
  return row;
}

}