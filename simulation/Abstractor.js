//Saikrishnan Ranganathan U096089W
//Tey Jun Hong U095074X
/*
 Compression and Decompression of game objects done in this class
 */

var abstractor = {
    doMagic:function (objectToAbstract, objectToDeabstract) {

        var abstractToLevel2 = function (objectToAbstract) {
            var abstractedLevel2Data = {};

            var level1Data = simulationArray.level1;
            var policeCount = level1Data.police.length;
            var robberCount = level1Data.robber.length;
            var dealerCount = level1Data.dealer.length;

            var candy1Count = 0;
            var candy2Count = 0;
            var candy3Count = 0;
            var candy4Count = 0;

            var rumourACount = 0;
            var rumourBCount = 0;
            var rumourCCount = 0;
            var rumourDCount = 0;

            for (var i = 0; i < level1Data.dealer.length; i++) {
                candy1Count += level1Data.dealer[i].candy1;
                candy2Count += level1Data.dealer[i].candy2;
                candy3Count += level1Data.dealer[i].candy3;
                candy4Count += level1Data.dealer[i].candy4;
                rumourACount += level1Data.dealer[i].rumourAcount;
                rumourBCount += level1Data.dealer[i].rumourBcount;
                rumourCCount += level1Data.dealer[i].rumourCcount;
                rumourDCount += level1Data.dealer[i].rumourDcount;
            }
            characterInfo = {
                dealer:dealerCount,
                police:policeCount,
                robber:robberCount
            };

            candyInfo = {
                candy1:candy1Count,
                candy2:candy2Count,
                candy3:candy3Count,
                candy4:candy4Count
            };

            rumourInfo = {
                rumourA:rumourACount,
                rumourB:rumourBCount,
                rumourC:rumourCCount,
                rumourD:rumourDCount
            };

            abstractedLevel2Data.abstractionLevel = 2;
            abstractedLevel2Data.id = objectToAbstract.id;
            abstractedLevel2Data.type = objectToAbstract.type;
            if (objectToAbstract.type === "pub") {
                abstractedLevel2Data.parentTownId = objectToAbstract.parentTownId;
            }
            abstractedLevel2Data.character = characterInfo;
            abstractedLevel2Data.candy = candyInfo;
            abstractedLevel2Data.rumour = rumourInfo;

            return abstractedLevel2Data;
        };

        var abstractToLevel3 = function (objectToAbstract) {
            var abstractedLevel3Data = {};

            var mostPromCandy = objectToAbstract.candy["candy1"];
			
            for (var candyKey in objectToAbstract.candy) {
                var candy = objectToAbstract.candy[candyKey];
                if (candy >= mostPromCandy) {
                    //alert("mostprocan" + mostPromCandy);
                    if(candyKey == 'candy1'){
                    	mostPromCandy = 1;                   
                    }
                    if(candyKey == 'candy2'){
                    	mostPromCandy = 2;                   
                    }
                    if(candyKey == 'candy3'){
                    	mostPromCandy = 3;                   
                    }
                    if(candyKey == 'candy4'){
                    	mostPromCandy = 4;                   
                    }
                }
            }

            var mostPromRumour = objectToAbstract.rumour["rumourA"];

            for (var rumourKey in objectToAbstract.rumour) {
                var rumour = objectToAbstract.rumour[rumourKey];
                if (rumour >= mostPromRumour) {
                    //alert("mostprocan" + mostPromRumour);
                    if(rumourKey == 'rumourA'){
                    	mostPromRumour = 1;                   
                    }
                    if(rumourKey == 'rumourB'){
                    	mostPromRumour = 2;                   
                    }
                    if(candyKey == 'rumourC'){
                    	mostPromRumour = 3;                   
                    }
                    if(candyKey == 'rumourD'){
                    	mostPromRumour = 4;                   
                    }
                }
            }

            abstractedLevel3Data.abstractionLevel = 3;
            abstractedLevel3Data.id = objectToAbstract.id;
            abstractedLevel3Data.type = objectToAbstract.type;
            if (objectToAbstract.type === "pub") {
                abstractedLevel3Data.parentTownId = objectToAbstract.parentTownId;
            }
            abstractedLevel3Data.promCandy = mostPromCandy;
            abstractedLevel3Data.promRumour = mostPromRumour;

            return abstractedLevel3Data;
        };

        var deabstractToLevel2 = function (objectToDeabstract) {

            var mostPromCandy = objectToDeabstract.promCandy;
            var mostPromRumour = objectToDeabstract.promRumour;
            var candy1 = 0;
            var candy2 = 0;
            var candy3 = 0;
            var candy4 = 0;
            var rumourA = 0;
            var rumourB = 0;
            var rumourC = 0;
            var rumourD = 0;

            //People quota
            //City: 30
            //Town: 20
            //Pub: 10
            for (var i = 0; i < simulationArray.level3.length; i++) {
                if (simulationArray.level3[i].promCandy === 1) {
                    candy1++;
                }
                if (simulationArray.level3[i].promCandy === 2) {
                    candy2++;
                }
                if (simulationArray.level3[i].promCandy === 3) {
                    candy3++;
                }
                if (simulationArray.level3[i].promCandy === 4) {
                    candy4++;
                }
                if (simulationArray.level3[i].promRumour === 1) {
                    rumourA++;
                }
                if (simulationArray.level3[i].promRumour === 2) {
                    rumourB++;
                }
                if (simulationArray.level3[i].promRumour === 3) {
                    rumourC++;
                }
                if (simulationArray.level3[i].promRumour === 4) {
                    rumourD++;
                }
            }

            if (objectToDeabstract.type === "city") {
                characterInfo = {
                    dealer:20,
                    police:4,
                    robber:4
                };
            } else if (objectToDeabstract.type === "town") {
                characterInfo = {
                    dealer:15,
                    police:3,
                    robber:3
                };
            } else if (objectToDeabstract.type === "pub") {
                characterInfo = {
                    dealer:10,
                    police:2,
                    robber:2
                };
            }

            candyInfo = {
                candy1:candy1,
                candy2:candy2,
                candy3:candy3,
                candy4:candy4
            };

            rumourInfo = {
                rumourA:rumourA,
                rumourB:rumourB,
                rumourC:rumourC,
                rumourD:rumourD
            };

            var deabstractedLevel2Object = {};

            deabstractedLevel2Object.abstractionLevel = 2;
            deabstractedLevel2Object.id = objectToDeabstract.id;
            deabstractedLevel2Object.type = objectToDeabstract.type;
            if (objectToDeabstract.type === "pub") {
                deabstractedLevel2Object.parentTownId = objectToDeabstract.parentTownId;
            }
            deabstractedLevel2Object.character = characterInfo;
            deabstractedLevel2Object.candy = candyInfo;
            deabstractedLevel2Object.rumour = rumourInfo;

            return deabstractedLevel2Object;
        };

        var deabstractToLevel1 = function (objectToDeabstract) {
            /*The main objective of NB in this case is to find the deviation of the parameters of the new level 1
             from the initial values of the parameter of level 1
             Thus, we have a level 2 data set to work with and a level 1 data set to work with.
             */
            /*
             Level 2 data set will be Object to de-abstract
             Level 1 data set will be Object that is currently in level 1
             */
            /*
             Initial Level 2 parameters:
             Character Ratio: 5:1:1 of people quota //Pub x 2, Town x 3, City x 4
             Candy Ratio: 1:1:1:1
             Rumour Ratio: 1:1:1:1
             */
            /*
             Initial Level 1 parameters:
             this.rumourAcount = 0;	// Number of times the person is in contact with rumour A
             this.rumourBcount = 0;
             this.rumourCcount = 0;
             this.rumourDcount = 0;
             this.rumourTheshold = 10;
             this.rumourProb: 50%
             this.candy1 = 50;	// Quantity of candies
             this.candy2 = 0;
             this.candy3 = 0;
             this.candy4 = 0;
             this.candy1Price = 50;	// Quantity of candies
             this.candy2Price = 50;
             this.candy3Price = 50;
             this.candy4Price = 50;
             this.bribeAmount = 100;			// Bribe amount of police
             this.stealAmount = 5;	// The amount to steal
             this.stealType = 0;		// 4 robber each own type
             */
            // We get level 2 data first and map it to a level 1
            var L2policeCount = 0;
            var L2robberCount = 0;
            var L2dealerCount = 0;

            var L2candy1Count = 0;
            var L2candy2Count = 0;
            var L2candy3Count = 0;
            var L2candy4Count = 0;

            var L2rumourACount = 0;
            var L2rumourBCount = 0;
            var L2rumourCCount = 0;
            var L2rumourDCount = 0;

            for (var candyKey in objectToDeabstract.candy) {
                var candy = objectToDeabstract.candy[candyKey];
                if (candyKey == "candy1") {
                    L2candy1Count = candy;
                } else if (candyKey == "candy2") {
                    L2candy2Count = candy;
                } else if (candyKey == "candy3") {
                    L2candy3Count = candy;
                } else if (candyKey == "candy4") {
                    L2candy4Count = candy;
                }
            }

            for (var rumourKey in objectToDeabstract.rumour) {
                var rumour = objectToDeabstract.rumour[rumourKey];
                if (rumourKey == "rumourA") {
                    L2rumourACount = rumour;
                } else if (rumourKey == "rumourB") {
                    L2rumourBCount = rumour;
                } else if (rumourKey == "rumourC") {
                    L2rumourCCount = rumour;
                } else if (rumourKey == "rumourD") {
                    L2rumourDCount = rumour;
                }
            }

            for (var characterKey in objectToDeabstract.character) {
                var character = objectToDeabstract.character[characterKey];
                if (characterKey == "dealer") {
                    L2dealerCount = character;
                } else if (characterKey == "robber") {
                    L2robberCount = character;
                } else if (characterKey == "police") {
                    L2policeCount = character;
                }
            }

            // Now to do a comparison of level2 data and how it deviation from the norm
            // We are not concerned with level1 data for comparison as the case now is L2-L1
            // However we need L1 data for other purposes later
            var sumCharacter = L2dealerCount + L2robberCount + L2policeCount;
            var policeDeviation = 1 / 7 - L2policeCount / sumCharacter;
            var robberDeviation = 1 / 7 - L2robberCount / sumCharacter;
            var dealerDeviation = 5 / 7 - L2dealerCount / sumCharacter;

            var sumCandy = L2candy1Count + L2candy2Count + L2candy3Count + L2candy4Count;
            var candy1Deviation = 1 / 4 - L2candy1Count / sumCandy;
            var candy2Deviation = 1 / 4 - L2candy2Count / sumCandy;
            var candy3Deviation = 1 / 4 - L2candy3Count / sumCandy;
            var candy4Deviation = 1 / 4 - L2candy4Count / sumCandy;

            var sumRumour = L2rumourACount + L2rumourBCount + L2rumourCCount + L2rumourDCount;
            var rumourADeviation = 1 / 4 - L2rumourACount / sumRumour;
            var rumourBDeviation = 1 / 4 - L2rumourBCount / sumRumour;
            var rumourCDeviation = 1 / 4 - L2rumourCCount / sumRumour;
            var rumourDDeviation = 1 / 4 - L2rumourDCount / sumRumour;

            // We get level 1 data for each OBJ first to compare it with the initial
            // this is current level 1 right? why are we using this data?
            var level1Data = simulationArray.level1;

            var newLevel1Data = {};
            newLevel1Data.dealer = [];
            newLevel1Data.police = [];
            newLevel1Data.robber = [];

			var minCharacterCount = Math.min(level1Data.police.length, level1Data.robber.length, level1Data.dealer.length);
			if(objectToDeabstract.type === "city"){
				var characterRatio = 4;
			}else if(objectToDeabstract.type === "town"){
				var characterRatio = 3;
			}else if(objectToDeabstract.type === "pub"){
				var characterRatio = 2;
			}
			var L1policeCount = Math.floor(level1Data.police.length/minCharacterCount) * characterRatio;
			var L1robberCount = Math.floor(level1Data.robber.length/minCharacterCount) * characterRatio;
			var L1dealerCount = Math.floor(level1Data.dealer.length/minCharacterCount) * characterRatio;		

            var L1candy1Count = 50;
            var L1candy2Count = 50;
            var L1candy3Count = 50;
            var L1candy4Count = 50;

            var L1rumourACount = 0;
            var L1rumourBCount = 0;
            var L1rumourCCount = 0;
            var L1rumourDCount = 0;

            // Now to include the deviation from earlier into the L1 data
            L1policeCount = Math.floor(L1policeCount + policeDeviation * L1policeCount);
            L1robberCount = Math.floor(L1robberCount + robberDeviation * L1robberCount);
            L1dealerCount = Math.floor(L1dealerCount + dealerDeviation * L1dealerCount);

            function clone(obj){
                if(obj == null || typeof(obj) != 'object')
                    return obj;

                var temp = obj.constructor();

                for(var key in obj)
                    temp[key] = clone(obj[key]);
                return temp;
            }
			var averageDeviationOfRumours = (rumourADeviation + rumourBDeviation + rumourCDeviation + rumourDDeviation) / 4;
            /******
             Here lies the problem, I highlight the problem where due to Index, it is very hard to assign the data to
             the objects on the map
             Now, there is character count change and now u need to identify the character becoz
             the deviation will be added to each character
             My solution is to add an ID to each object but due to complexity of Azri code, that might be alot of work
             ******/

            //level1Data.dealer.length should be the new Count after character changes due to quota
            // Eg, when moving from town to pub, the quota should be smaller
            for (var i = 0; i < L1dealerCount; i++) {
            	if(i >= level1Data.dealer.length){
            	    spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
            		spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1); 
             		newLevel1Data.dealer.push(new DealerObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"dealer"));           			
					if(i % 4 == 0){
						newLevel1Data.dealer[i].rumourAcount = 5;
						newLevel1Data.dealer[i].candy1 = 50;		
						newLevel1Data.dealer[i].rumourBcount = 1;
						newLevel1Data.dealer[i].candy2 = 10;			
						newLevel1Data.dealer[i].rumourCcount = 1;
						newLevel1Data.dealer[i].candy3 = 10;			
						newLevel1Data.dealer[i].rumourDcount = 1;	
						newLevel1Data.dealer[i].candy4 = 10;
						newLevel1Data.dealer[i].candy1Price = Math.floor(50 + 50 * rumourADeviation);	
						newLevel1Data.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
						newLevel1Data.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
						newLevel1Data.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);					
					}
					if(i % 4 == 1){
						newLevel1Data.dealer[i].rumourAcount = 1;
						newLevel1Data.dealer[i].candy1 = 10;	
						newLevel1Data.dealer[i].rumourBcount = 5;
						newLevel1Data.dealer[i].candy2 = 50;			
						newLevel1Data.dealer[i].rumourCcount = 1;
						newLevel1Data.dealer[i].candy3 = 10;			
						newLevel1Data.dealer[i].rumourDcount = 1;	
						newLevel1Data.dealer[i].candy4 = 10;
						newLevel1Data.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
						newLevel1Data.dealer[i].candy2Price = Math.floor(50 + 50 * rumourBDeviation);	
						newLevel1Data.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
						newLevel1Data.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);							
					}
					if(i % 4 == 2){
						newLevel1Data.dealer[i].rumourAcount = 1;
						newLevel1Data.dealer[i].candy1 = 10;		
						newLevel1Data.dealer[i].rumourBcount = 1;
						newLevel1Data.dealer[i].candy2 = 10;			
						newLevel1Data.dealer[i].rumourCcount = 5;
						newLevel1Data.dealer[i].candy3 = 50;			
						newLevel1Data.dealer[i].rumourDcount = 1;	
						newLevel1Data.dealer[i].candy4 = 10;
						newLevel1Data.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
						newLevel1Data.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
						newLevel1Data.dealer[i].candy3Price = Math.floor(50 + 50 * rumourCDeviation);	
						newLevel1Data.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);							
					}
					if(i % 4 == 3){
						newLevel1Data.dealer[i].rumourAcount = 1;
						newLevel1Data.dealer[i].candy1 = 10;		
						newLevel1Data.dealer[i].rumourBcount = 1;
						newLevel1Data.dealer[i].candy2 = 10;			
						newLevel1Data.dealer[i].rumourCcount = 1;
						newLevel1Data.dealer[i].candy3 = 10;			
						newLevel1Data.dealer[i].rumourDcount = 5;	
						newLevel1Data.dealer[i].candy4 = 50;
						newLevel1Data.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
						newLevel1Data.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
						newLevel1Data.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
						newLevel1Data.dealer[i].candy4Price = Math.floor(50 + 50 * rumourDDeviation);							
					}		
					newLevel1Data.dealer[i].rumourThres = 10;
					newLevel1Data.dealer[i].rumourProb = 0.5;	
            	}else{
					L1candy1Count = level1Data.dealer[i].candy1;
					L1candy2Count = level1Data.dealer[i].candy2;
					L1candy3Count = level1Data.dealer[i].candy3;
					L1candy4Count = level1Data.dealer[i].candy4;
					L1rumourACount = level1Data.dealer[i].rumourAcount;
					L1rumourBCount = level1Data.dealer[i].rumourBcount;
					L1rumourCCount = level1Data.dealer[i].rumourCcount;
					L1rumourDCount = level1Data.dealer[i].rumourDcount;
				

					// create object in new level1 data by cloning existing one
					newLevel1Data.dealer.push(clone(level1Data.dealer[i]));

					// Assigning back the new candy and rumour count to each dealer
					// Note that there might be new dealer
					newLevel1Data.dealer[i].candy1 = Math.abs(Math.floor(L1candy1Count + candy1Deviation));
					newLevel1Data.dealer[i].candy2 = Math.abs(Math.floor(L1candy2Count + candy2Deviation));
					newLevel1Data.dealer[i].candy3 = Math.abs(Math.floor(L1candy3Count + candy3Deviation));
					newLevel1Data.dealer[i].candy4 = Math.abs(Math.floor(L1candy4Count + candy4Deviation));

					newLevel1Data.dealer[i].rumourAcount = (L1rumourACount + rumourADeviation * L1rumourACount)%5;
					newLevel1Data.dealer[i].rumourBcount = (L1rumourBCount + rumourBDeviation * L1rumourBCount)%5;
					newLevel1Data.dealer[i].rumourCcount = (L1rumourCCount + rumourCDeviation * L1rumourCCount)%5;
					newLevel1Data.dealer[i].rumourDcount = (L1rumourDCount + rumourDDeviation * L1rumourDCount)%5;
					
					newLevel1Data.dealer[i].rumourThres = 10;
					//newLevel1Data.dealer[i].rumourThres += averageDeviationOfRumours * level1Data.dealer[i].rumourThres;
					newLevel1Data.dealer[i].rumourProb = 0.5;
					//newLevel1Data.dealer[i].rumourProb += averageDeviationOfRumours * level1Data.dealer[i].rumourProb;
					//newLevel1Data.dealer[i].rumourProb = Math.min(level1Data.dealer[i].rumourProb, 0);

					newLevel1Data.dealer[i].candy1Price += Math.floor(level1Data.dealer[i].candy1Price * rumourADeviation);
					newLevel1Data.dealer[i].candy2Price += Math.floor(level1Data.dealer[i].candy2Price * rumourBDeviation);
					newLevel1Data.dealer[i].candy3Price += Math.floor(level1Data.dealer[i].candy3Price * rumourCDeviation);
					newLevel1Data.dealer[i].candy4Price += Math.floor(level1Data.dealer[i].candy4Price * rumourDDeviation);
					// Refresh if necessary
					if(newLevel1Data.dealer[i].candy1Price == 0){
						newLevel1Data.dealer[i].candy1Price = 50;
					}
					if(newLevel1Data.dealer[i].candy2Price == 0){
						newLevel1Data.dealer[i].candy2Price = 50;
					}
					if(newLevel1Data.dealer[i].candy3Price == 0){
						newLevel1Data.dealer[i].candy3Price = 50;
					}
					if(newLevel1Data.dealer[i].candy4Price == 0){
						newLevel1Data.dealer[i].candy4Price = 50;
					}
				}
            }

            for (var i = 0; i < L1policeCount; i++) {
             	if(i >= level1Data.police.length){
            	    spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
            		spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1);              	
					newLevel1Data.police.push(new PoliceObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"police"));            	
             		newLevel1Data.police[i].bribeAmount = 100 + 100 * averageDeviationOfRumours;
             	}else{
                	newLevel1Data.police.push(clone(level1Data.police[i]));
                	newLevel1Data.police[i].bribeAmount += level1Data.police[i].bribeAmount * averageDeviationOfRumours;
				}
                // Same thing for police
                //this.bribeAmount = 100;			// Bribe amount of police
            }

            for (var i = 0; i < L1robberCount; i++) {
              	if(i >= level1Data.robber.length){ 
            	    spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
            		spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1); 
                    newLevel1Data.robber.push(new RobberObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"robber"));   
					newLevel1Data.robber[i].stealAmount = 5 + 5 * averageDeviationOfRumours;
					newLevel1Data.robber[i].stealType = i%4;
              	}else{       
					newLevel1Data.robber.push(clone(level1Data.robber[i]));
					newLevel1Data.robber[i].stealAmount += level1Data.robber[i].stealAmount * averageDeviationOfRumours;
					newLevel1Data.robber[i].stealType = 1;

					if (L1rumourBCount > L1rumourACount) {
						newLevel1Data.robber[i].stealType = 2;
					} else if (L1rumourCCount > L1rumourBCount) {
						newLevel1Data.robber[i].stealType = 3;
					} else if (L1rumourDCount > L1rumourCCount) {
						newLevel1Data.robber[i].stealType = 4;
					}
				}

                // Same thing for robber
                //this.stealAmount = 5;	// The amount to steal
                //this.stealType = 0;
            }


            // Essentially, NB above made use of only 1 level 2data set to find the probable value for the level1
            // This is ok, as it is an interrupt
            // When doing polling. ie, progagation across all the level 2 array and level 3 array,
            // There will be more dataset to based on.
            // The deviation during polling will normalise the data across all the level 2 obj


            newLevel1Data.abstractionLevel = 1;
            newLevel1Data.id = objectToDeabstract.id;
            newLevel1Data.type = objectToDeabstract.type;
            if (objectToDeabstract.type === "pub") {
                newLevel1Data.parentTownId = objectToDeabstract.parentTownId;
            }

            return newLevel1Data;
        };

		//console.log("before");
        //console.log(simulationArray.level1);

        // remove this from level 2
        var newLevel1 = deabstractToLevel1(objectToDeabstract);

        //console.log("after");
        //console.log(newLevel1);

        //console.log("object to abstract type" + objectToAbstract.type);


        // now move objects around levels based on where we are on level 1
		// If going to pub
        if (newLevel1.type === "pub") {
            oldLevel1 = simulationArray.level1; // town which this pub is in
            oldLevel2 = simulationArray.level2; // city, this pub & other pubs in this town
            oldLevel3 = simulationArray.level3; // pubs in other towns and other towns

            newLevel2 = []; // town which this pub is in
            newLevel3 = []; // city and other towns and other pubs


            newLevel2.push(abstractToLevel2(oldLevel1));


            for (i = 0; i < oldLevel2.length; i++) {
                if (oldLevel2[i].type === "pub" && oldLevel2[i].id !== newLevel1.id) {
                    newLevel3.push(abstractToLevel3(oldLevel2[i]));
                }
                if (oldLevel2[i].type === "city") {
                    newLevel3.push(abstractToLevel3(oldLevel2[i]));                
                }
             }

            // concat oldLevel3 to newLevel3, since same abstraction
            newLevel3.push.apply(newLevel3, oldLevel3);

            simulationArray.level1 = newLevel1;
            simulationArray.level2 = newLevel2;
            simulationArray.level3 = newLevel3;

        }
		// If going town or city
        if (newLevel1.type === "town" && objectToAbstract.type === "city") {

            //console.log("enter town from city");
            // check if coming in from city or pub
            oldLevel1 = simulationArray.level1; //city
            oldLevel2 = simulationArray.level2; // all towns
            oldLevel3 = simulationArray.level3; // all pubs

            newLevel2 = []; // city, pubs in this town
            newLevel3 = []; // other towns and pubs in other towns


            newLevel2.push(abstractToLevel2(oldLevel1));


            for (var i = 0; i < oldLevel3.length; i++) {
                if (oldLevel3[i].parentTownId === newLevel1.id) {
                    newLevel2.push(deabstractToLevel2(oldLevel3[i]));
                } else if (oldLevel3[i].parentTownId !== newLevel1.id) {
                    newLevel3.push(oldLevel3[i]);
                }
            }

            for (var i = 0; i < oldLevel2.length; i++) {
                if (oldLevel2.id !== newLevel1.id) {
                    newLevel3.push(abstractToLevel3(oldLevel2[i]));
                }
            }

            simulationArray.level1 = newLevel1;
            simulationArray.level2 = newLevel2;
            simulationArray.level3 = newLevel3;

           	//console.log("after doing shifting");
            //console.log(simulationArray);
        }

        if (newLevel1.type === "town" && objectToAbstract.type === "pub") {
            // check if coming in from city or pub
            oldLevel1 = simulationArray.level1; // pub am leaving from
            oldLevel2 = simulationArray.level2; // town in which old pub is located
            oldLevel3 = simulationArray.level3; // city and other pubs in town and other towns and pubs in other towns

            newLevel2 = []; // city, pubs in this town
            newLevel3 = []; // other towns and pubs in other towns


            newLevel2.push(abstractToLevel2(oldLevel1));


            for (var i = 0; i < oldLevel3.length; i++) {
                if (oldLevel3[i].type === "city") { //city
                    newLevel2.push(deabstractToLevel2(oldLevel3[i]));
                } else if (oldLevel3[i].type === "town" && oldLevel3[i].id !== newLevel1.id) { // other towns
                    newLevel3.push(oldLevel3[i]);
                } else if (oldLevel3[i].type === "pub" && oldLevel3[i].parentTownId === newLevel1.id) { //pubs in this town
                    newLevel2.push(deabstractToLevel2(oldLevel3[i]));
                } else if (oldLevel3[i].type === "pub" && oldLevel3[i].parentTownId !== newLevel1.id) { //pubs in other town
                    newLevel3.push(oldLevel3[i]);
                }
            }

            simulationArray.level1 = newLevel1;
            simulationArray.level2 = newLevel2;
            simulationArray.level3 = newLevel3;
        }

        if (newLevel1.type === "city") {
            oldLevel1 = simulationArray.level1; // town I exited
            oldLevel2 = simulationArray.level2; // city and other pubs in old town
            oldLevel3 = simulationArray.level3; // other towns and pubs in other towns

            newLevel2 = []; // towns
            newLevel3 = []; // pubs in towns

            newLevel2.push(abstractToLevel2(oldLevel1));


            for (var i = 0; i < oldLevel2.length; i++) {
                if (oldLevel2[i].type === "pub") {
                    newLevel3.push(abstractToLevel3(oldLevel2[i]));
                }
            }

            for (var i = 0; i < oldLevel3.length; i++) {
                if (oldLevel3[i].type === "town" && oldLevel3[i].id!==oldLevel1.id) {
                    newLevel2.push(deabstractToLevel2(oldLevel3[i]));
                } else if (oldLevel3[i].type === "pub") {
                    newLevel3.push(oldLevel3[i]);
                }
            }

            simulationArray.level1 = newLevel1;
            simulationArray.level2 = newLevel2;
            simulationArray.level3 = newLevel3;
        }

        return newLevel1;

    }
};

