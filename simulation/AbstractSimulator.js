//Saikrishnan Ranganathan U096089W
//Tey Jun Hong U095074X
/*
 Simulate the abstract data structures here
 */

var abstractSimulator = {

    // do this check if necessary before updating global simulationArray to make sure
    // data we store at start of function call has not become stale
    globalDataValidation:function () {

    },

    // call this function when someone leaves level1
    level1PersonLeaveTrigger:function (person, whereHeIsGoing) {
		// Find the immediate level 2 obj
        function getObjectHeIsGoingTo(type, id, parentTownId) {
            for (var i = 0; i < simulationArray.level2.length; i++) {
                if (simulationArray.level2[i].type !== "pub" && simulationArray.level2[i].type === type && simulationArray.level2[i].id === id) {
                    return simulationArray.level2[i];
                } else if (simulationArray.level2[i].type === "pub" && simulationArray.level2[i].id === id &&
                    simulationArray.level2[i].parentTownId === parentTownId) {
                    return simulationArray.level2[i];
                }
            }
        }

        var level2ObjectGoingTo = getObjectHeIsGoingTo(whereHeIsGoing.type, whereHeIsGoing.id, whereHeIsGoing.parentTownId);

        // update simulationArray.level2 statistics
        if (person.id === "robber") {
            level2ObjectGoingTo.character.robber++;
            // add the robber parameter
        } else if (person.id === "police") {
            level2ObjectGoingTo.character.police++;
            // add the police parameter 
        } else if (person.id === "dealer") {
            level2ObjectGoingTo.character.dealer++;

            // update candy and rumour stuff
            level2ObjectGoingTo.candy.candy1+=person.candy1;
            level2ObjectGoingTo.candy.candy2+=person.candy2;
            level2ObjectGoingTo.candy.candy3+=person.candy3;
            level2ObjectGoingTo.candy.candy4+=person.candy4;

            level2ObjectGoingTo.rumour.rumourA+=person.rumourAcount;
            level2ObjectGoingTo.rumour.rumourB+=person.rumourBcount;
            level2ObjectGoingTo.rumour.rumourC+=person.rumourCcount;
            level2ObjectGoingTo.rumour.rumourD+=person.rumourDcount;
        }     
        
        // Update the new simulation array level 1 with new level 2 datas
        var aveRobberCount = 0;
        var avePoliceCount = 0;
        var aveDealerCount = 0;
        var aveCandy1Count = 0;
        var aveCandy2Count = 0;
        var aveCandy3Count = 0;
        var aveCandy4Count = 0;
        var aveRumourACount = 0;
        var aveRumourBCount = 0;
        var aveRumourCCount = 0;
        var aveRumourDCount = 0;
        for(var i = 0; i < simulationArray.level2.length; i++){
        	aveRobberCount += simulationArray.level2[i].character.robber;
        	avePoliceCount += simulationArray.level2[i].character.police;
        	aveDealerCount += simulationArray.level2[i].character.dealer;
        	aveCandy1Count += simulationArray.level2[i].candy.candy1;
        	aveCandy2Count += simulationArray.level2[i].candy.candy2;
        	aveCandy3Count += simulationArray.level2[i].candy.candy3;
        	aveCandy4Count += simulationArray.level2[i].candy.candy4;
        	aveRumourACount += simulationArray.level2[i].rumour.rumourA;  
        	aveRumourBCount += simulationArray.level2[i].rumour.rumourB;  
        	aveRumourCCount += simulationArray.level2[i].rumour.rumourC;  
        	aveRumourDCount += simulationArray.level2[i].rumour.rumourD;        	
        }
        aveRobberCount = aveRobberCount/simulationArray.level2.length;
        avePoliceCount = avePoliceCount/simulationArray.level2.length;
        aveDealerCount = aveDealerCount/simulationArray.level2.length;
        aveCandy1Count = aveCandy1Count/simulationArray.level2.length;
        aveCandy2Count = aveCandy2Count/simulationArray.level2.length;
        aveCandy3Count = aveCandy3Count/simulationArray.level2.length;
        aveCandy4Count = aveCandy4Count/simulationArray.level2.length;
        aveRumourACount = aveRumourACount/simulationArray.level2.length;
        aveRumourBCount = aveRumourBCount/simulationArray.level2.length;
        aveRumourCCount = aveRumourCCount/simulationArray.level2.length;
        aveRumourDCount = aveRumourDCount/simulationArray.level2.length;
        
		var sumCharacter = aveDealerCount + aveRobberCount + avePoliceCount;
		var policeDeviation = 1 / 7 - avePoliceCount / sumCharacter;
		var robberDeviation = 1 / 7 - aveRobberCount / sumCharacter;
		var dealerDeviation = 5 / 7 - aveDealerCount / sumCharacter;

		var sumCandy = aveCandy1Count + aveCandy2Count + aveCandy3Count + aveCandy4Count;
		var candy1Deviation = 1 / 4 - aveCandy1Count / sumCandy;
		var candy2Deviation = 1 / 4 - aveCandy2Count / sumCandy;
		var candy3Deviation = 1 / 4 - aveCandy3Count / sumCandy;
		var candy4Deviation = 1 / 4 - aveCandy4Count / sumCandy;

		var sumRumour = aveRumourACount + aveRumourBCount + aveRumourCCount + aveRumourDCount;
		var rumourADeviation = 1 / 4 - aveRumourACount / sumRumour;
		var rumourBDeviation = 1 / 4 - aveRumourBCount / sumRumour;
		var rumourCDeviation = 1 / 4 - aveRumourCCount / sumRumour;
		var rumourDDeviation = 1 / 4 - aveRumourDCount / sumRumour;
		
		var level1Data = simulationArray.level1;

		var minCharacterCount = Math.min(level1Data.police.length, level1Data.robber.length, level1Data.dealer.length);
		if(level2ObjectGoingTo.type === "city"){
			var characterRatio = 4;
		}else if(level2ObjectGoingTo.type === "town"){
			var characterRatio = 3;
		}else if(level2ObjectGoingTo.type === "pub"){
			var characterRatio = 2;
		}
		var L1policeCount = Math.floor(level1Data.police.length/minCharacterCount) * characterRatio;
		var L1robberCount = Math.floor(level1Data.robber.length/minCharacterCount) * characterRatio;
		var L1dealerCount = Math.floor(level1Data.dealer.length/minCharacterCount) * characterRatio;		

		var L1candy1Count = 50;
		var L1candy2Count = 50;
		var L1candy3Count = 50;
		var L1candy4Count = 50;

		var L1rumourACount = 0;
		var L1rumourBCount = 0;
		var L1rumourCCount = 0;
		var L1rumourDCount = 0;

		// Now to include the deviation from earlier into the L1 data
		L1policeCount = Math.floor(L1policeCount + policeDeviation * L1policeCount);
		L1robberCount = Math.floor(L1robberCount + robberDeviation * L1robberCount);
		L1dealerCount = Math.floor(L1dealerCount + dealerDeviation * L1dealerCount);

		var averageDeviationOfRumours = (rumourADeviation + rumourBDeviation + rumourCDeviation + rumourDDeviation) / 4;
		
		// If the characters count is greater than original, push
		for (var i = 0; i < L1dealerCount; i++) {
			// Push the new objs
			if(i >= level1Data.dealer.length){
				spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
				spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1); 
				simulationArray.level1.dealer.push(new DealerObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"dealer"));           			
				if(i % 4 == 0){
					simulationArray.level1.dealer[i].rumourAcount = 5;
					simulationArray.level1.dealer[i].candy1 = 50;		
					simulationArray.level1.dealer[i].rumourBcount = 1;
					simulationArray.level1.dealer[i].candy2 = 10;			
					simulationArray.level1.dealer[i].rumourCcount = 1;
					simulationArray.level1.dealer[i].candy3 = 10;			
					simulationArray.level1.dealer[i].rumourDcount = 1;	
					simulationArray.level1.dealer[i].candy4 = 10;
					simulationArray.level1.dealer[i].candy1Price = Math.floor(50 + 50 * rumourADeviation);	
					simulationArray.level1.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
					simulationArray.level1.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
					simulationArray.level1.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);					
				}
				if(i % 4 == 1){
					simulationArray.level1.dealer[i].rumourAcount = 1;
					simulationArray.level1.dealer[i].candy1 = 10;	
					simulationArray.level1.dealer[i].rumourBcount = 5;
					simulationArray.level1.dealer[i].candy2 = 50;			
					simulationArray.level1.dealer[i].rumourCcount = 1;
					simulationArray.level1.dealer[i].candy3 = 10;			
					simulationArray.level1.dealer[i].rumourDcount = 1;	
					simulationArray.level1.dealer[i].candy4 = 10;
					simulationArray.level1.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
					simulationArray.level1.dealer[i].candy2Price = Math.floor(50 + 50 * rumourBDeviation);	
					simulationArray.level1.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
					simulationArray.level1.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);							
				}
				if(i % 4 == 2){
					simulationArray.level1.dealer[i].rumourAcount = 1;
					simulationArray.level1.dealer[i].candy1 = 10;		
					simulationArray.level1.dealer[i].rumourBcount = 1;
					simulationArray.level1.dealer[i].candy2 = 10;			
					simulationArray.level1.dealer[i].rumourCcount = 5;
					simulationArray.level1.dealer[i].candy3 = 50;			
					simulationArray.level1.dealer[i].rumourDcount = 1;	
					simulationArray.level1.dealer[i].candy4 = 10;
					simulationArray.level1.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
					simulationArray.level1.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
					simulationArray.level1.dealer[i].candy3Price = Math.floor(50 + 50 * rumourCDeviation);	
					simulationArray.level1.dealer[i].candy4Price = Math.floor(10 + 10 * rumourDDeviation);							
				}
				if(i % 4 == 3){
					simulationArray.level1.dealer[i].rumourAcount = 1;
					simulationArray.level1.dealer[i].candy1 = 10;		
					simulationArray.level1.dealer[i].rumourBcount = 1;
					simulationArray.level1.dealer[i].candy2 = 10;			
					simulationArray.level1.dealer[i].rumourCcount = 1;
					simulationArray.level1.dealer[i].candy3 = 10;			
					simulationArray.level1.dealer[i].rumourDcount = 5;	
					simulationArray.level1.dealer[i].candy4 = 50;
					simulationArray.level1.dealer[i].candy1Price = Math.floor(10 + 10 * rumourADeviation);	
					simulationArray.level1.dealer[i].candy2Price = Math.floor(10 + 10 * rumourBDeviation);	
					simulationArray.level1.dealer[i].candy3Price = Math.floor(10 + 10 * rumourCDeviation);	
					simulationArray.level1.dealer[i].candy4Price = Math.floor(50 + 50 * rumourDDeviation);							
				}		
				simulationArray.level1.dealer[i].rumourThres = 10;
				simulationArray.level1.dealer[i].rumourProb = 0.5;	
			}
		}

		for (var i = 0; i < L1policeCount; i++) {
			if(i >= level1Data.police.length){
				spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
				spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1);              	
				simulationArray.level1.police.push(new PoliceObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"police"));            	
				simulationArray.level1.police[i].bribeAmount = 100 + 100 * averageDeviationOfRumours;
			}
		}

		for (var i = 0; i < L1robberCount; i++) {
			if(i >= level1Data.robber.length){ 
				spawnRow = Math.floor(Math.random() * (gameboard.rows-1))+1;
				spawnCol = Math.floor(Math.random() * (gameboard.columns-1)+1); 
				simulationArray.level1.robber.push(new RobberObj(map[spawnRow][spawnCol].pos.pos_x,map[spawnRow][spawnCol].pos.pos_y,20,"robber"));   
				simulationArray.level1.robber[i].stealAmount = 5 + 5 * averageDeviationOfRumours;
				simulationArray.level1.robber[i].stealType = i%4;
			}
		}	
		
		//else if the character count is less than the original count	
		if(L1dealerCount < level1Data.dealer.length){		
			for (var i = L1dealerCount; i < level1Data.dealer.length; i++) {
				// POP the array
				simulationArray.level1.dealer.pop();
			}
		}
		
		if(L1robberCount < level1Data.robber.length){		
			for (var i = L1robberCount; i < level1Data.robber.length; i++) {
				// POP the array
				simulationArray.level1.robber.pop();
			}
		}
		
		if(L1policeCount < level1Data.police.length){		
			for (var i = L1policeCount; i < level1Data.police.length; i++) {
				// POP the array
				simulationArray.level1.police.pop();
			}
		}


    },
    // call this function as part of the render loop (requestAnimeFrame) to simulate spread and stuff on level 2 and 3
    simulate:function () {

        function level2Sim() {
            // update level 2 data here based on spread model, so rumour and candy counts and stuff changes here
            // All the levels two in fact are 1 to 2 portal away from each other
            // Since the size of level 2 is small, we can assume they are al neighbouring states
			for(var i = 0; i < simulationArray.level2.length; i++){
				var L2policeCount = simulationArray.level2[i].character.police;
				var L2robberCount = simulationArray.level2[i].character.robber;
				var L2dealerCount = simulationArray.level2[i].character.dealer;

				var L2candy1Count = simulationArray.level2[i].candy.candy1;
				var L2candy2Count = simulationArray.level2[i].candy.candy2;
				var L2candy3Count = simulationArray.level2[i].candy.candy3;
				var L2candy4Count = simulationArray.level2[i].candy.candy4;

				var L2rumourACount = simulationArray.level2[i].rumour.rumourA;
				var L2rumourBCount = simulationArray.level2[i].rumour.rumourB;
				var L2rumourCCount = simulationArray.level2[i].rumour.rumourC;
				var L2rumourDCount = simulationArray.level2[i].rumour.rumourD;
			
				var sumCharacter = L2dealerCount + L2robberCount + L2policeCount;
				var policeDeviation = 1 / 7 - L2policeCount / sumCharacter;
				var robberDeviation = 1 / 7 - L2robberCount / sumCharacter;
				var dealerDeviation = 5 / 7 - L2dealerCount / sumCharacter;

				var sumCandy = L2candy1Count + L2candy2Count + L2candy3Count + L2candy4Count;
				var candy1Deviation = 1 / 4 - L2candy1Count / sumCandy;
				var candy2Deviation = 1 / 4 - L2candy2Count / sumCandy;
				var candy3Deviation = 1 / 4 - L2candy3Count / sumCandy;
				var candy4Deviation = 1 / 4 - L2candy4Count / sumCandy;

				var sumRumour = L2rumourACount + L2rumourBCount + L2rumourCCount + L2rumourDCount;
				var rumourADeviation = 1 / 4 - L2rumourACount / sumRumour;
				var rumourBDeviation = 1 / 4 - L2rumourBCount / sumRumour;
				var rumourCDeviation = 1 / 4 - L2rumourCCount / sumRumour;
				var rumourDDeviation = 1 / 4 - L2rumourDCount / sumRumour;
				
				// Update the resultant parameters
				// We always try to normalise it to NB slowly
				// First the character
				if(policeDeviation > 0){
					simulationArray.level2[i].character.police += policeDeviation * 0.1; 
				}else{
					simulationArray.level2[i].character.police -= policeDeviation * 0.1; 					
				}	
				if(dealerDeviation > 0){
					simulationArray.level2[i].character.dealer += dealerDeviation * 0.1; 
				}else{
					simulationArray.level2[i].character.dealer -= dealerDeviation * 0.1; 					
				}	
				if(robberDeviation > 0){
					simulationArray.level2[i].character.robber += robberDeviation * 0.1; 
				}else{
					simulationArray.level2[i].character.robber -= robberDeviation * 0.1; 					
				}		
				// Second the candy
				if(candy1Deviation > 0){
					simulationArray.level2[i].candy.candy1 += candy1Deviation * 0.1; 
				}else{
					simulationArray.level2[i].candy.candy1 -= candy1Deviation * 0.1; 					
				}	
				if(candy2Deviation > 0){
					simulationArray.level2[i].candy.candy2 += candy2Deviation * 0.1; 
				}else{
					simulationArray.level2[i].candy.candy2 -= candy2Deviation * 0.1; 					
				}	
				if(candy3Deviation > 0){
					simulationArray.level2[i].candy.candy3 += candy3Deviation * 0.1; 
				}else{
					simulationArray.level2[i].candy.candy3 -= candy3Deviation * 0.1; 					
				}	
				if(candy4Deviation > 0){
					simulationArray.level2[i].candy.candy4 += candy4Deviation * 0.1; 
				}else{
					simulationArray.level2[i].candy.candy4 -= candy4Deviation * 0.1; 					
				}	
				// Third the rumours
				if(rumourADeviation > 0){
					simulationArray.level2[i].rumour.rumourA += rumourADeviation * 0.1; 
				}else{
					simulationArray.level2[i].rumour.rumourA -= rumourADeviation * 0.1; 					
				}	
				if(rumourBDeviation > 0){
					simulationArray.level2[i].rumour.rumourB += rumourBDeviation * 0.1; 
				}else{
					simulationArray.level2[i].rumour.rumourB -= rumourBDeviation * 0.1; 					
				}
				if(rumourCDeviation > 0){
					simulationArray.level2[i].rumour.rumourC += rumourCDeviation * 0.1; 
				}else{
					simulationArray.level2[i].rumour.rumourC -= rumourCDeviation * 0.1; 					
				}	
				if(rumourDDeviation > 0){
					simulationArray.level2[i].rumour.rumourD += rumourDDeviation * 0.1; 
				}else{
					simulationArray.level2[i].rumour.rumourD -= rumourDDeviation * 0.1; 					
				}								
			}

            // if number of dealer/police/robber falls below threshold set by us, then push new dealer/police/robber
            // to the simulationArray.level1.dealer/police/robber.
            // set the attributes based on level2 data count and stuff
			/*	Not necessary anymore as it is done above by a per entry basis instead of polling 

			
            var thresholdDealer;
            var thresholdPolice;
            var thresholdRobber;

            if(simulationArray.level1.dealer.length < thresholdDealer){
                // determine the parameters through NB on level 2 data, Tey intervene here also :P
                var newDealerToPush = new DealerObj();

                simulationArray.level1.dealer.push(newDealerToPush);
            }

            if(simulationArray.level1.police.length < thresholdPolice){
                // determine the parameters through NB on level 2 data, Tey intervene here also :P
                var newPoliceToPush = new PoliceObj();

                simulationArray.level1.police.push(newPoliceToPush);
            }

            if(simulationArray.level1.robber.length < thresholdRobber){
                // determine the parameters through NB on level 2 data, Tey intervene here also :P
                var newRobberToPush = new RobberObj();

                simulationArray.level1.robber.push(newRobberToPush);
            }
            */

        }

        function level3Sim() {
            // based on changes in level 2 data, set most prom rumour and candy
            // if rumour count and candy count in level2 changes and overtakes current one, change the prominent one
            var L3candy1Count = 0;
            var L3candy2Count = 0;
            var L3candy3Count = 0;
            var L3candy4Count = 0;
            var L3rumourACount = 0;
            var L3rumourBCount = 0;
            var L3rumourCCount = 0;
            var L3rumourDCount = 0;
            var winCandy = 0;
            var winRumour = 0;
			for(var i = 0; i < simulationArray.level3.length; i++){	
				if(simulationArray.level3[i].promCandy == 1){
					L3candy1Count ++;
				}
				if(simulationArray.level3[i].promCandy == 2){
					L3candy2Count ++;
				}
				if(simulationArray.level3[i].promCandy == 3){
					L3candy3Count ++;
				}
				if(simulationArray.level3[i].promCandy == 4){
					L3candy4Count ++;
				}
				if(simulationArray.level3[i].promRumour == 1){
					L3rumourACount ++;
				}
				if(simulationArray.level3[i].promRumour == 2){
					L3rumourBCount ++;
				}
				if(simulationArray.level3[i].promRumour == 3){
					L3rumourCCount ++;
				}
				if(simulationArray.level3[i].promRumour == 4){
					L3rumourDCount ++;
				}			
			}	
			var winCandyCount = Math.max(L3candy1Count,L3candy2Count, L3candy3Count, L3candy4Count);	
			var winRumourCount = Math.max(L3rumourACount,L3rumourBCount, L3rumourCCount, L3rumourDCount);
			if(L3candy1Count == winCandyCount){
				winCandy = 1;
			}
			if(L3candy2Count == winCandyCount){
				winCandy = 2;
			}
			if(L3candy3Count == winCandyCount){
				winCandy = 3;
			}
			if(L3candy4Count == winCandyCount){
				winCandy = 4;
			}
			if(L3rumourACount == winRumourCount){
				winRumour = 1;
			}
			if(L3rumourBCount == winRumourCount){
				winRumour = 2;
			}
			if(L3rumourCCount == winRumourCount){
				winRumour = 3;
			}
			if(L3rumourDCount == winRumourCount){
				winRumour = 4;
			}
			
			for(var i = 0; i < simulationArray.level3.length; i++){	
				var fate = Math.random();
				if(fate > 0.5){
					simulationArray.level3.promCandy = winCandy;
				}else if(fate > 0.4){
					simulationArray.level3.promCandy = 1;
				}else if(fate > 0.3){
					simulationArray.level3.promCandy = 2;
				}else if(fate > 0.2){
					simulationArray.level3.promCandy = 3;
				}else if(fate > 0.1){
					simulationArray.level3.promCandy = 4;
				}
				
				if(fate > 0.5){
					simulationArray.level3.promRumour = winRumour;
				}else if(fate > 0.4){
					simulationArray.level3.promRumour = 1;
				}else if(fate > 0.3){
					simulationArray.level3.promRumour = 2;
				}else if(fate > 0.2){
					simulationArray.level3.promRumour = 3;
				}else if(fate > 0.1){
					simulationArray.level3.promRumour = 4;
				}
			}			
            // needs loop
            // var currentMostPromRumour = simulationArray.level3;
        }

        // perform simulation

        level2Sim();
        level3Sim();
    }

};