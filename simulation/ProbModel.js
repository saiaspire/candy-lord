//Arison Neo A0077784L
function numberOfRumorsHeard(int timeElasped){

	int totalPopulation = 20;
	int spreadRumourProb = ((1/totalPopulation)*ln(12/9)) // 12/9 is based on 0.25 prob of spreading rumour when meeting someone else
	
	var heard = totalPopulation * ((exp(totalPopulation*spreadRumourProb*timeElasped)) / (4+exp(totalPopulation*spreadRumourProb*timeElasped)));
	
	return heard;
	
}

//function to decide which rumour to accept
function rumourAcceptance(int rumourA, int rumourB, int rumourC, int rumourD) {
	
	if (rumourA > rumourB && rumourA > rumourC && rumourA > rumourD)
	{
	getRumour();
	}
	else if (rumourB > rumourA && rumourB > rumourC && rumourB > rumourD)
	{
	getRumour();
	}
	else if (rumourC > rumourA && rumourC > rumourB && rumourC > rumourD)
	{
	getRumour();
	}
	else if (rumourD > rumourA && rumourD > rumourB && rumourD > rumourC)
	{
	getRumour();
	}
	
}

